# Changelog

## 1.0.1 (2024-07-14) - Small improvements

* More detailed error messages
* GitLab unit test report upload fixed
* Test coverage

## 1.0.0 (2024-06-21) - Initial version

* Expressions with most common arithmetic, bit and logic operators
