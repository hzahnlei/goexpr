package main

import (
	"bufio"
	"fmt"
	"io"
	"os"

	"zahnleiter.org/goexpr/pkg/eval"
	"zahnleiter.org/goexpr/pkg/expression"
	"zahnleiter.org/goexpr/pkg/formatter"
	"zahnleiter.org/goexpr/pkg/lexical"
	"zahnleiter.org/goexpr/pkg/syntactical"
)

func main() {
	executionContext := eval.NewMapBasedVisitorContext(nil)
	eval.RegisterBuiltinFunctions(&executionContext)
	eval.RegisterBuiltinConstants(&executionContext)
	reader := bufio.NewReader(os.Stdin)
	for {
		text, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				// This is OK
				os.Exit(EXIT_OK)
			}
			panic(err)
		}
		if err := interpret(text, &executionContext); err != nil {
			fmt.Fprintln(os.Stderr, err.Error())
			os.Exit(EXIT_ERROR)
		}
	}
}

const (
	EXIT_OK int = iota
	EXIT_ERROR
)

func interpret(text string, executionContext expression.VisitorContext) error {
	source := lexical.NewSource(text, "")
	scanner, err := lexical.NewScanner(source)
	if err != nil {
		return err
	}
	parser, err := syntactical.NewParser(scanner)
	if err != nil {
		return err
	}
	for parser.Available() {
		expr := parser.Peek()
		result, err := eval.Evaluate(expr, executionContext)
		if err != nil {
			return err
		}
		printer := formatter.NewResultPrinter()
		fmt.Println(printer.Format(result))
		if err = parser.Advance(); err != nil {
			return err
		}
	}
	return nil
}
