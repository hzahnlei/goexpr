package goexpr_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"zahnleiter.org/goexpr/pkg/eval"
)

// ---- SPEP test = scan, parse, eval print test ------------------------------

func TestSPEPShortestPossibleSequence(t *testing.T) {
	ctx := eval.NewMapBasedParentVisitorContext()
	assert.Equal(t, "100", evaluateWithContext("a := 10; a * a", &ctx))
}

func TestSPEPBitLongerSequence(t *testing.T) {
	ctx := eval.NewMapBasedParentVisitorContext()
	assert.Equal(t, "110", evaluateWithContext("a := 10; b:=11; a * b", &ctx))
}

func TestSPEPDefinitionsDoNotLeakSequence(t *testing.T) {
	ctx := eval.NewMapBasedParentVisitorContext()
	assert.Equal(t, "110", evaluateWithContext("a := 10; b:=11; a * b", &ctx))
	assert.Equal(t, `Runtime error at line 1, column 1: Identifier "a" is not defined.`, evaluateWithContext("a", &ctx))
	assert.Equal(t, `Runtime error at line 1, column 1: Identifier "b" is not defined.`, evaluateWithContext("b", &ctx))
}

func TestSPEPWithMoreElements(t *testing.T) {
	ctx := eval.NewMapBasedParentVisitorContext()
	// Do not expect 1320! Remember these are uint8!!!
	assert.Equal(t, "40", evaluateWithContext("a:=10; b:=11; c:=12; a*b*c", &ctx))
	assert.Equal(t, `Runtime error at line 1, column 1: Identifier "a" is not defined.`, evaluateWithContext("a", &ctx))
	assert.Equal(t, `Runtime error at line 1, column 1: Identifier "b" is not defined.`, evaluateWithContext("b", &ctx))
	assert.Equal(t, `Runtime error at line 1, column 1: Identifier "c" is not defined.`, evaluateWithContext("c", &ctx))
}

func TestSPEPGlobalsCanBeDefinedOnTopLevel(t *testing.T) {
	ctx := eval.NewMapBasedParentVisitorContext()
	assert.Equal(t, "10", evaluateWithContext("a := 10", &ctx))
	assert.Equal(t, "11", evaluateWithContext("b := 11", &ctx))
	assert.Equal(t, "110", evaluateWithContext("a*b", &ctx))
}

func TestSPEPNestedSequence(t *testing.T) {
	ctx := eval.NewMapBasedParentVisitorContext()
	assert.Equal(t, "550", evaluateWithContext("a:=10u16; b:=11u16; c:=(x:=2u16;y:=3u16;x+y); a*b*c", &ctx))
	assert.Equal(t, `Runtime error at line 1, column 1: Identifier "a" is not defined.`, evaluateWithContext("a", &ctx))
	assert.Equal(t, `Runtime error at line 1, column 1: Identifier "b" is not defined.`, evaluateWithContext("b", &ctx))
	assert.Equal(t, `Runtime error at line 1, column 1: Identifier "c" is not defined.`, evaluateWithContext("c", &ctx))
	assert.Equal(t, `Runtime error at line 1, column 1: Identifier "x" is not defined.`, evaluateWithContext("x", &ctx))
	assert.Equal(t, `Runtime error at line 1, column 1: Identifier "y" is not defined.`, evaluateWithContext("y", &ctx))
}

func TestSPEPNestedScopeOfInnerSequenceDoesNotLeak(t *testing.T) {
	ctx := eval.NewMapBasedParentVisitorContext()
	assert.Equal(t,
		`evaluating sequence element #3: Runtime error at line 1, column 47: Identifier "x" is not defined.`,
		evaluateWithContext("a:=10u16; b:=11u16; c:=(x:=2u16;y:=3u16;x+y); x+y", &ctx))
}
