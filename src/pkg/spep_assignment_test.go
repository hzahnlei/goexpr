package goexpr_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/goexpr/pkg/eval"
)

// ---- SPEP test = scan, parse, eval print test ------------------------------

func TestSPEPAssignmentOfSInt(t *testing.T) {
	ctx := eval.NewMapBasedVisitorContext(nil)
	assert.Equal(t, "10S", evaluateWithContext("a := 10S", &ctx))
	val, found := ctx.Lookup("a")
	require.True(t, found)
	assert.Equal(t, "10s8", val.String())
}

func TestSPEPAssignmentOfUInt(t *testing.T) {
	ctx := eval.NewMapBasedVisitorContext(nil)
	assert.Equal(t, "10U16", evaluateWithContext("a := 10U16", &ctx))
	val, found := ctx.Lookup("a")
	require.True(t, found)
	assert.Equal(t, "10u16", val.String())
}

func TestSPEPAssignmentOfComplexExpression(t *testing.T) {
	ctx := eval.NewMapBasedVisitorContext(nil)
	assert.Equal(t, "23", evaluateWithContext("a := 3 + 4 * 5", &ctx))
	val, found := ctx.Lookup("a")
	require.True(t, found)
	assert.Equal(t, "23u8", val.String())
}

func TestSPEPAssignmentOfFloat(t *testing.T) {
	ctx := eval.NewMapBasedVisitorContext(nil)
	assert.Equal(t, "10.160000", evaluateWithContext("a := 10.16", &ctx))
	val, found := ctx.Lookup("a")
	require.True(t, found)
	assert.Equal(t, "10.160000", val.String())
}
