package goexpr_test

import (
	"testing"

	"github.com/stretchr/testify/require"
)

// ---- SPEP test = scan, parse, eval print test ------------------------------

func TestSPEPEmptyInput(t *testing.T) {
	// GIVEN
	source := ""
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "", result)
}

func TestSPEPUnknownSymbol(t *testing.T) {
	// GIVEN
	source := "§"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "Lexical error at line 1, column 1: Unexpected character '§'.", result)
}
