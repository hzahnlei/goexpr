package goexpr_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// ---- SPEP test = scan, parse, eval print test ------------------------------

func TestSPEPRelationalOperators(t *testing.T) {
	assert.Equal(t, "1", evaluateWithoutContext("1 == 1"))
	assert.Equal(t, "0", evaluateWithoutContext("1 == 2"))
	assert.Equal(t, "0", evaluateWithoutContext("2 == 1"))
	assert.Equal(t, "1", evaluateWithoutContext("2 == 2"))

	assert.Equal(t, "1", evaluateWithoutContext("1 >= 1"))
	assert.Equal(t, "0", evaluateWithoutContext("1 >= 2"))
	assert.Equal(t, "1", evaluateWithoutContext("2 >= 1"))
	assert.Equal(t, "1", evaluateWithoutContext("2 >= 2"))

	assert.Equal(t, "0", evaluateWithoutContext("1 >  1"))
	assert.Equal(t, "0", evaluateWithoutContext("1 >  2"))
	assert.Equal(t, "1", evaluateWithoutContext("2 >  1"))
	assert.Equal(t, "0", evaluateWithoutContext("2 >  2"))

	assert.Equal(t, "1", evaluateWithoutContext("1 <= 1"))
	assert.Equal(t, "1", evaluateWithoutContext("1 <= 2"))
	assert.Equal(t, "0", evaluateWithoutContext("2 <= 1"))
	assert.Equal(t, "1", evaluateWithoutContext("2 <= 2"))

	assert.Equal(t, "0", evaluateWithoutContext("1 <  1"))
	assert.Equal(t, "1", evaluateWithoutContext("1 <  2"))
	assert.Equal(t, "0", evaluateWithoutContext("2 <  1"))
	assert.Equal(t, "0", evaluateWithoutContext("2 <  2"))

	assert.Equal(t, "0", evaluateWithoutContext("1 != 1"))
	assert.Equal(t, "1", evaluateWithoutContext("1 != 2"))
	assert.Equal(t, "1", evaluateWithoutContext("2 != 1"))
	assert.Equal(t, "0", evaluateWithoutContext("2 != 2"))
}
