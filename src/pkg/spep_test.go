package goexpr_test

import (
	"zahnleiter.org/goexpr/pkg/eval"
	"zahnleiter.org/goexpr/pkg/expression"
	"zahnleiter.org/goexpr/pkg/formatter"
	"zahnleiter.org/goexpr/pkg/lexical"
	"zahnleiter.org/goexpr/pkg/syntactical"
)

// ---- SPEP test = scan, parse, eval print test ------------------------------

func evaluateWithoutContext(expr string) string {
	return evaluateWithContext(expr, nil)
}

func evaluateWithDefaultContext(expr string) string {
	ctx := eval.NewMapBasedParentVisitorContext()
	eval.RegisterBuiltinFunctions(&ctx)
	return evaluateWithContext(expr, &ctx)
}

func evaluateWithContext(expr string, ctx expression.VisitorContext) string {
	source := lexical.NewSource(expr, "")
	scanner, err := lexical.NewScanner(source)
	if err != nil {
		return err.Error()
	}
	parser, err := syntactical.NewParser(scanner)
	if err != nil {
		return err.Error()
	}
	result, err := eval.Evaluate(parser.Peek(), ctx)
	if err != nil {
		return err.Error()
	}
	formatter := formatter.NewResultPrinter()
	return formatter.Format(result)
}
