package lexical

import "unicode/utf8"

type Source struct {
	content      []rune
	runeCount    int
	index        int
	currLocation Location
}

func NewSource(content string, sourceName string) Source {
	return Source{
		content:   []rune(content),
		runeCount: utf8.RuneCountInString(content),
		index:     0,
		currLocation: Location{
			SourceName: sourceName,
			Line:       1,
			Column:     1,
		},
	}
}

type Location struct {
	SourceName string
	Line       int
	Column     int
}

func (src *Source) Available() bool {
	return src.index < src.runeCount
}

func (src *Source) Peek() rune {
	if src.Available() {
		return src.content[src.index]
	}
	return noChar
}

const noChar rune = '\x00'

func (src *Source) ConsumeCurrent() {
	if src.Available() {
		char := src.content[src.index]
		src.index++
		if char == '\n' {
			src.currLocation.Column = 1
			src.currLocation.Line++
		} else {
			src.currLocation.Column++
		}
	}
}

func (src *Source) CurrentLocation() Location {
	return src.currLocation
}
