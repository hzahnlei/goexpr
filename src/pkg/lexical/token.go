package lexical

type Token struct {
	Lexeme   string
	Kind     TokenType
	Location Location
}

type TokenType int

const (
	TokenEOF             TokenType = iota
	TokenArithDivide               // /
	TokenArithMinus                // -
	TokenArithModulus              // %
	TokenArithMultiply             // *
	TokenArithPlus                 // +
	TokenArithPower                // **
	TokenArithShiftLeft            // <<
	TokenArithShiftRight           // >>
	TokenAssignment                // :=
	TokenBitAnd                    // &
	TokenBitNot                    // ~
	TokenBitOr                     // |
	TokenBitXOr                    // ^
	TokenBoolAnd                   // &&
	TokenBoolNot                   // !
	TokenBoolOr                    // ||
	TokenBoolXOr                   // ^^
	TokenIdentifier                // abc, AbCd, ab_b_cd
	TokenLiteralSInt8              // 123S8, 0x59S8, 0b1010s8
	TokenLiteralSInt16             // 123S16, 0xaffeS16, 0b1010S16
	TokenLiteralSInt32             // 123S32, 0xaffeS32, 0b1010s32
	TokenLiteralSInt64             // 123S64, 0xaffeS64, 0b1010s64
	TokenLiteralSInt               // 123s, 0xaffeS, 0b1010s -> Size of literal decides about type
	TokenLiteralUInt8              // 123U8, 0x59U8, 0b1010u8
	TokenLiteralUInt16             // 123u16, 0xaffeU16, 0b1010U16
	TokenLiteralUInt32             // 123U32, 0xaffeU32, 0b1010u32
	TokenLiteralUInt64             // 123U64, 0xaffeU64, 0b1010u64
	TokenLiteralUInt               // 123, 123u, 0xaffe, 0xaffeU, 0b1010 -> Size of literal decides about type
	TokenParenLeft                 // (
	TokenParenRight                // )
	TokenRelEQ                     // ==
	TokenRelGE                     // >=
	TokenRelGT                     // >
	TokenRelLE                     // <=
	TokenRelLT                     // <
	TokenRelNE                     // !=
	TokenSemicolon                 // ;
	TokenComma                     // ,
	TokenLiteralFloat              // 123.45
)

func (token Token) IsA(tokenTypes ...TokenType) bool {
	for _, tokenType := range tokenTypes {
		if token.Kind == tokenType {
			return true
		}
	}
	return false
}
