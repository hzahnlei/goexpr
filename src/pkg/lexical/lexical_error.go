package lexical

import "zahnleiter.org/goexpr/pkg/commons"

func lexicalError(msg string, location Location) error {
	return commons.MustNewLexicalError(msg, location.SourceName, location.Line, location.Column)
}
