package lexical

import (
	"fmt"
	"strings"
	"unicode"
)

type Scanner struct {
	source    Source
	currToken Token
	mark      Location
}

func NewScanner(source Source) (Scanner, error) {
	scanner := Scanner{
		source: source,
		mark:   source.CurrentLocation(),
	}
	err := scanner.Advance()
	return scanner, err
}

func (scn *Scanner) Peek() Token {
	return scn.currToken
}

func (scn *Scanner) Available() bool {
	return scn.source.Available()
}

func (scn *Scanner) Advance() error {
	for scn.source.Available() {
		scn.markCurrentLocation()
		currChar := scn.source.Peek()
		scn.source.ConsumeCurrent()
		switch currChar {
		//---- Arithmetic operators ----
		case '/':
			scn.setCurrenToken(TokenArithDivide)
			return nil
		case '-':
			scn.setCurrenToken(TokenArithMinus)
			return nil
		case '%':
			scn.setCurrenToken(TokenArithModulus)
			return nil
		case '*':
			scn.scanForMulOrPow()
			return nil
		case '+':
			scn.setCurrenToken(TokenArithPlus)
			return nil
		//---- Assignment operator ----
		case ':':
			return scn.scanForAssignementOperator()
		//---- Bitwise and boolean operators ----
		case '&':
			scn.scanForBitOrBooleanAnd()
			return nil
		case '~':
			scn.setCurrenToken(TokenBitNot)
			return nil
		case '!':
			scn.scanForBooleanNotOrNE()
			return nil
		case '|':
			scn.scanForBitOrBooleanOr()
			return nil
		case '^':
			scn.scanForBitOrBooleanXOr()
			return nil
		case '(':
			scn.setCurrenToken(TokenParenLeft)
			return nil
		case ')':
			scn.setCurrenToken(TokenParenRight)
			return nil
		//---- Releational or arithmetic operators ----
		case '=':
			return scn.scanForEQ()
		case '<':
			scn.scanForLTorLEorSL()
			return nil
		case '>':
			scn.scanForGTorGEorSR()
			return nil
		//---- Sequence and list operators ----
		case ';':
			scn.setCurrenToken(TokenSemicolon)
			return nil
		case ',':
			scn.setCurrenToken(TokenComma)
			return nil
		//---- Identifiers and literals ----
		default:
			if unicode.IsDigit(currChar) {
				return scn.scanForNumber(currChar)
			} else if currChar == '_' || unicode.IsLetter(currChar) {
				return scn.scanForIdentifier(currChar)
			} else if unicode.IsSpace(currChar) {
				continue
			} else {
				return lexicalError(
					fmt.Sprintf("Unexpected character %q.", currChar),
					scn.mark)
			}
		}
	}
	scn.setCurrenToken(TokenEOF)
	return nil
}

func (scn *Scanner) markCurrentLocation() {
	scn.mark = scn.source.CurrentLocation()
}

func (scn *Scanner) setCurrenToken(kind TokenType) {
	scn.currToken.Kind = kind
	scn.currToken.Lexeme = lexemeMapping[kind]
	scn.currToken.Location = scn.mark
}

var lexemeMapping = map[TokenType]string{
	TokenArithDivide:     "/",
	TokenArithMinus:      "-",
	TokenArithModulus:    "%",
	TokenArithMultiply:   "*",
	TokenArithPlus:       "+",
	TokenArithPower:      "**",
	TokenArithShiftLeft:  "<<",
	TokenArithShiftRight: ">>",
	TokenAssignment:      ":=",
	TokenBitAnd:          "&",
	TokenBitNot:          "~",
	TokenBitOr:           "|",
	TokenBitXOr:          "^",
	TokenBoolAnd:         "&&",
	TokenBoolNot:         "!",
	TokenBoolOr:          "||",
	TokenBoolXOr:         "^^",
	TokenParenLeft:       "(",
	TokenParenRight:      ")",
	TokenRelEQ:           "==",
	TokenRelGE:           ">=",
	TokenRelGT:           ">",
	TokenRelLE:           "<=",
	TokenRelLT:           "<",
	TokenRelNE:           "!=",
	TokenSemicolon:       ";",
	TokenComma:           ",",
}

func (scn *Scanner) scanForMulOrPow() {
	if scn.source.Peek() == '*' {
		scn.source.ConsumeCurrent()
		scn.setCurrenToken(TokenArithPower)
	} else {
		scn.setCurrenToken(TokenArithMultiply)
	}
}

func (scn *Scanner) scanForAssignementOperator() error {
	if scn.source.Peek() == '=' {
		scn.source.ConsumeCurrent()
		scn.setCurrenToken(TokenAssignment)
		return nil
	}
	return lexicalError(
		fmt.Sprintf("Do not know how to handle ':'. Expected '=' but found %q.", scn.source.Peek()),
		scn.source.currLocation)
}

func (scn *Scanner) scanForBitOrBooleanAnd() {
	if scn.source.Peek() == '&' {
		scn.source.ConsumeCurrent()
		scn.setCurrenToken(TokenBoolAnd)
	} else {
		scn.setCurrenToken(TokenBitAnd)
	}
}

func (scn *Scanner) scanForBooleanNotOrNE() {
	if scn.source.Peek() == '=' {
		scn.source.ConsumeCurrent()
		scn.setCurrenToken(TokenRelNE)
	} else {
		scn.setCurrenToken(TokenBoolNot)
	}
}

func (scn *Scanner) scanForEQ() error {
	if scn.source.Peek() == '=' {
		scn.source.ConsumeCurrent()
		scn.setCurrenToken(TokenRelEQ)
		return nil
	} else if !scn.source.Available() {
		return lexicalError(
			fmt.Sprintf("Premature end of equality operator %q. Expected another %q but reached end of file.",
				lexemeMapping[TokenRelEQ], "="),
			scn.source.CurrentLocation())
	} else {
		return lexicalError(
			fmt.Sprintf("Premature end of equality operator %q. Expected another %q but found %q.",
				lexemeMapping[TokenRelEQ], "=", scn.source.Peek()),
			scn.source.CurrentLocation())
	}
}

func (scn *Scanner) scanForLTorLEorSL() {
	if scn.source.Peek() == '=' {
		scn.source.ConsumeCurrent()
		scn.setCurrenToken(TokenRelLE)
	} else if scn.source.Peek() == '<' {
		scn.source.ConsumeCurrent()
		scn.setCurrenToken(TokenArithShiftLeft)
	} else {
		scn.setCurrenToken(TokenRelLT)
	}
}

func (scn *Scanner) scanForGTorGEorSR() {
	if scn.source.Peek() == '=' {
		scn.source.ConsumeCurrent()
		scn.setCurrenToken(TokenRelGE)
	} else if scn.source.Peek() == '>' {
		scn.source.ConsumeCurrent()
		scn.setCurrenToken(TokenArithShiftRight)
	} else {
		scn.setCurrenToken(TokenRelGT)
	}
}

func (scn *Scanner) scanForBitOrBooleanOr() {
	if scn.source.Peek() == '|' {
		scn.source.ConsumeCurrent()
		scn.setCurrenToken(TokenBoolOr)
	} else {
		scn.setCurrenToken(TokenBitOr)
	}
}

func (scn *Scanner) scanForBitOrBooleanXOr() {
	if scn.source.Peek() == '^' {
		scn.source.ConsumeCurrent()
		scn.setCurrenToken(TokenBoolXOr)
	} else {
		scn.setCurrenToken(TokenBitXOr)
	}
}

func (scn *Scanner) scanForNumber(currChar rune) error {
	var lexeme strings.Builder
	lexeme.WriteRune(currChar)
	if currChar == '0' {
		nextChar := scn.source.Peek()
		if nextChar == 'x' || nextChar == 'X' {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
			return scn.scanForHexNumber(&lexeme)
		} else if nextChar == 'b' || nextChar == 'B' {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
			return scn.scanForBinNumber(&lexeme)
		}
	}
	return scn.scanForDecNumber(&lexeme)
}

func (scn *Scanner) scanForHexNumber(lexeme *strings.Builder) error {
	if !scn.source.Available() {
		return lexicalError(
			fmt.Sprintf("Premature end of hexadecimal number literal %q.", lexeme.String()),
			scn.source.CurrentLocation())
	}
	for scn.source.Available() {
		nextChar := scn.source.Peek()
		if unicode.In(nextChar, unicode.ASCII_Hex_Digit) {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
		} else if nextChar == 'u' || nextChar == 'U' {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
			uintTokenType, err := scn.scanForUIntTokenType(lexeme)
			if err != nil {
				return err
			}
			scn.setCurrenTokenWithLexeme(uintTokenType, lexeme.String())
			return nil
		} else if nextChar == 's' || nextChar == 'S' {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
			sintTokenType, err := scn.scanForSIntTokenType(lexeme)
			if err != nil {
				return err
			}
			scn.setCurrenTokenWithLexeme(sintTokenType, lexeme.String())
			return nil
		} else {
			break
		}
	}
	scn.setCurrenTokenWithLexeme(TokenLiteralUInt, lexeme.String())
	return nil
}

func (scn *Scanner) scanForUIntTokenType(lexeme *strings.Builder) (TokenType, error) {
	switch scn.source.Peek() {
	case '8':
		scn.source.ConsumeCurrent() // Consume '8'
		lexeme.WriteRune('8')
		return TokenLiteralUInt8, nil
	case '1':
		scn.source.ConsumeCurrent() // Consume '1'
		if scn.source.Peek() != '6' {
			if !scn.source.Available() {
				return TokenEOF, lexicalError(
					"Premature end of 16 bit unsigned int. Expected '6' but reached end of file.",
					scn.source.CurrentLocation())
			}
			return TokenEOF, lexicalError(
				fmt.Sprintf("Corrupt suffix of 16 bit unsigned int. Expected '6' but found %q.", scn.source.Peek()),
				scn.source.CurrentLocation())
		}
		scn.source.ConsumeCurrent() // Consume '6'
		lexeme.WriteString("16")
		return TokenLiteralUInt16, nil
	case '3':
		scn.source.ConsumeCurrent() // Consume '3'
		if scn.source.Peek() != '2' {
			if !scn.source.Available() {
				return TokenEOF, lexicalError(
					"Premature end of 32 bit unsigned int. Expected '2' but reached end of file.",
					scn.source.CurrentLocation())
			}
			return TokenEOF, lexicalError(
				fmt.Sprintf("PCorrupt suffix of 32 bit unsigned int. Expected '2' but found %q.", scn.source.Peek()),
				scn.source.CurrentLocation())
		}
		scn.source.ConsumeCurrent() // Consume '2'
		lexeme.WriteString("32")
		return TokenLiteralUInt32, nil
	case '6':
		scn.source.ConsumeCurrent() // Consume '6'
		if scn.source.Peek() != '4' {
			if !scn.source.Available() {
				return TokenEOF, lexicalError(
					"Premature end of 64 bit unsigned int. Expected '4' but reached end of file.",
					scn.source.CurrentLocation())
			}
			return TokenEOF, lexicalError(
				fmt.Sprintf("Corrupt suffix of 64 bit unsigned int. Expected '4' but found %q.", scn.source.Peek()),
				scn.source.CurrentLocation())
		}
		lexeme.WriteString("64")
		scn.source.ConsumeCurrent() // Consume '4'
		return TokenLiteralUInt64, nil
	default:
		return TokenLiteralUInt, nil // The size of the literal determines the type
	}
}

func (scn *Scanner) scanForSIntTokenType(lexeme *strings.Builder) (TokenType, error) {
	switch scn.source.Peek() {
	case '8':
		scn.source.ConsumeCurrent() // Consume '8'
		lexeme.WriteRune('8')
		return TokenLiteralSInt8, nil
	case '1':
		scn.source.ConsumeCurrent() // Consume '1'
		if scn.source.Peek() != '6' {
			if !scn.source.Available() {
				return TokenEOF, lexicalError(
					"Premature end of 16 bit signed int. Expected '6' but reached end of file.",
					scn.source.CurrentLocation())
			}
			return TokenEOF, lexicalError(
				fmt.Sprintf("Corrupt suffix of 16 bit signed int. Expected '6' but found %q.", scn.source.Peek()),
				scn.source.CurrentLocation())
		}
		scn.source.ConsumeCurrent() // Consume '6'
		lexeme.WriteString("16")
		return TokenLiteralSInt16, nil
	case '3':
		scn.source.ConsumeCurrent() // Consume '3'
		if scn.source.Peek() != '2' {
			if !scn.source.Available() {
				return TokenEOF, lexicalError(
					"Premature end of 32 bit signed int. Expected '2' but reached end of file.",
					scn.source.CurrentLocation())
			}
			return TokenEOF, lexicalError(
				fmt.Sprintf("PCorrupt suffix of 32 bit signed int. Expected '2' but found %q.", scn.source.Peek()),
				scn.source.CurrentLocation())
		}
		scn.source.ConsumeCurrent() // Consume '2'
		lexeme.WriteString("32")
		return TokenLiteralSInt32, nil
	case '6':
		scn.source.ConsumeCurrent() // Consume '6'
		if scn.source.Peek() != '4' {
			if !scn.source.Available() {
				return TokenEOF, lexicalError(
					"Premature end of 64 bit signed int. Expected '4' but reached end of file.",
					scn.source.CurrentLocation())
			}
			return TokenEOF, lexicalError(
				fmt.Sprintf("Corrupt suffix of 64 bit signed int. Expected '4' but found %q.", scn.source.Peek()),
				scn.source.CurrentLocation())
		}
		lexeme.WriteString("64")
		scn.source.ConsumeCurrent() // Consume '4'
		return TokenLiteralSInt64, nil
	default:
		return TokenLiteralSInt, nil // The size of the literal determines the type
	}
}

func (scn *Scanner) setCurrenTokenWithLexeme(kind TokenType, lexeme string) {
	scn.currToken.Kind = kind
	scn.currToken.Lexeme = lexeme
	scn.currToken.Location = scn.mark
}

func (scn *Scanner) scanForBinNumber(lexeme *strings.Builder) error {
	if !scn.source.Available() {
		return lexicalError(
			fmt.Sprintf("Premature end of binary number literal %q.", lexeme.String()),
			scn.source.CurrentLocation())
	}
	for scn.source.Available() {
		nextChar := scn.source.Peek()
		if nextChar == '0' || nextChar == '1' {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
		} else if nextChar == 'u' || nextChar == 'U' {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
			uintTokenType, err := scn.scanForUIntTokenType(lexeme)
			if err != nil {
				return err
			}
			scn.setCurrenTokenWithLexeme(uintTokenType, lexeme.String())
			return nil
		} else if nextChar == 's' || nextChar == 'S' {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
			sintTokenType, err := scn.scanForSIntTokenType(lexeme)
			if err != nil {
				return err
			}
			scn.setCurrenTokenWithLexeme(sintTokenType, lexeme.String())
			return nil
		} else {
			break
		}
	}
	scn.setCurrenTokenWithLexeme(TokenLiteralUInt, lexeme.String())
	return nil
}

func (scn *Scanner) scanForDecNumber(lexeme *strings.Builder) error {
	for scn.source.Available() {
		nextChar := scn.source.Peek()
		if unicode.IsDigit(nextChar) {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
		} else if nextChar == 'u' || nextChar == 'U' {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
			uintTokenType, err := scn.scanForUIntTokenType(lexeme)
			if err != nil {
				return err
			}
			scn.setCurrenTokenWithLexeme(uintTokenType, lexeme.String())
			return nil
		} else if nextChar == 's' || nextChar == 'S' {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
			sintTokenType, err := scn.scanForSIntTokenType(lexeme)
			if err != nil {
				return err
			}
			scn.setCurrenTokenWithLexeme(sintTokenType, lexeme.String())
			return nil
		} else if nextChar == '.' {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
			return scn.scanForFloatNumber(lexeme)
		} else {
			break
		}
	}
	scn.setCurrenTokenWithLexeme(TokenLiteralUInt, lexeme.String())
	return nil
}

func (scn *Scanner) scanForFloatNumber(lexeme *strings.Builder) error {
	if !scn.source.Available() {
		return lexicalError(
			fmt.Sprintf("Premature end of floating point number literal %q. Expected digits after decimal point but reaced end of file.", lexeme.String()),
			scn.source.CurrentLocation())
	}
	for scn.source.Available() {
		nextChar := scn.source.Peek()
		if unicode.IsDigit(nextChar) {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
		} else {
			break
		}
	}
	scn.setCurrenTokenWithLexeme(TokenLiteralFloat, lexeme.String())
	return nil
}

func (scn *Scanner) scanForIdentifier(currChar rune) error {
	var lexeme strings.Builder
	lexeme.WriteRune(currChar)
	for scn.source.Available() {
		nextChar := scn.source.Peek()
		if nextChar == '_' || unicode.IsLetter(nextChar) || unicode.IsDigit(nextChar) {
			lexeme.WriteRune(nextChar)
			scn.source.ConsumeCurrent()
		} else {
			break
		}
	}
	scn.setCurrenTokenWithLexeme(TokenIdentifier, lexeme.String())
	return nil
}
