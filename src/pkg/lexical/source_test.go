package lexical

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNothingAvailableIfSourceEmpty(t *testing.T) {
	source := NewSource("", "file name")
	assert.False(t, source.Available())
	assert.Equal(t, 1, source.CurrentLocation().Column)
	assert.Equal(t, 1, source.CurrentLocation().Line)
	assert.Equal(t, "file name", source.CurrentLocation().SourceName)
	assert.Equal(t, noChar, source.Peek())
}

func TestSomethingAvailableIfSourceNotEmpty(t *testing.T) {
	source := NewSource("so stuff", "file name")
	assert.True(t, source.Available())
	assert.Equal(t, 1, source.CurrentLocation().Column)
	assert.Equal(t, 1, source.CurrentLocation().Line)
	assert.Equal(t, "file name", source.CurrentLocation().SourceName)
	assert.Equal(t, 's', source.Peek())
}

func TestConsumingAnEmptySourceDoesNothing(t *testing.T) {
	// GIVEN
	emptySource := NewSource("", "")
	// WHEN
	emptySource.ConsumeCurrent()
	// THEN
	assert.False(t, emptySource.Available())
	assert.Equal(t, 1, emptySource.CurrentLocation().Column)
	assert.Equal(t, 1, emptySource.CurrentLocation().Line)
	assert.Equal(t, "", emptySource.CurrentLocation().SourceName)
	assert.Equal(t, noChar, emptySource.Peek())
}

func TestASourceCanBeConsumed(t *testing.T) {
	source := NewSource("abc", "")

	require.True(t, source.Available())
	assert.Equal(t, 1, source.CurrentLocation().Column)
	assert.Equal(t, 1, source.CurrentLocation().Line)
	assert.Equal(t, 'a', source.Peek())
	source.ConsumeCurrent()

	require.True(t, source.Available())
	assert.Equal(t, 2, source.CurrentLocation().Column)
	assert.Equal(t, 1, source.CurrentLocation().Line)
	assert.Equal(t, 'b', source.Peek())
	source.ConsumeCurrent()

	require.True(t, source.Available())
	assert.Equal(t, 3, source.CurrentLocation().Column)
	assert.Equal(t, 1, source.CurrentLocation().Line)
	assert.Equal(t, 'c', source.Peek())
	source.ConsumeCurrent()

	require.False(t, source.Available())
}

func TestSourceDoesTrackColumnAndLine(t *testing.T) {
	source := NewSource("a\nb\ncd", "")

	require.True(t, source.Available())
	assert.Equal(t, 1, source.CurrentLocation().Column)
	assert.Equal(t, 1, source.CurrentLocation().Line)
	assert.Equal(t, 'a', source.Peek())
	source.ConsumeCurrent()

	require.True(t, source.Available())
	assert.Equal(t, 2, source.CurrentLocation().Column)
	assert.Equal(t, 1, source.CurrentLocation().Line)
	assert.Equal(t, '\n', source.Peek())
	source.ConsumeCurrent()

	require.True(t, source.Available())
	assert.Equal(t, 1, source.CurrentLocation().Column)
	assert.Equal(t, 2, source.CurrentLocation().Line)
	assert.Equal(t, 'b', source.Peek())
	source.ConsumeCurrent()

	require.True(t, source.Available())
	assert.Equal(t, 2, source.CurrentLocation().Column)
	assert.Equal(t, 2, source.CurrentLocation().Line)
	assert.Equal(t, '\n', source.Peek())
	source.ConsumeCurrent()

	require.True(t, source.Available())
	assert.Equal(t, 1, source.CurrentLocation().Column)
	assert.Equal(t, 3, source.CurrentLocation().Line)
	assert.Equal(t, 'c', source.Peek())
	source.ConsumeCurrent()

	require.True(t, source.Available())
	assert.Equal(t, 2, source.CurrentLocation().Column)
	assert.Equal(t, 3, source.CurrentLocation().Line)
	assert.Equal(t, 'd', source.Peek())
	source.ConsumeCurrent()

	require.False(t, source.Available())
}
