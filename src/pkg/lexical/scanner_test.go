package lexical

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

//---- Integer literals -------------------------------------------------------

func TestScannerRecognizesBinaryLiterals(t *testing.T) {
	source := NewSource("0b1010 0B11001010", "")
	scanner, err := NewScanner(source)
	require.NoError(t, err)

	currToken := scanner.Peek()
	require.Equal(t, TokenLiteralUInt, currToken.Kind)
	assert.Equal(t, "0b1010", currToken.Lexeme)
	assert.Equal(t, 1, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenLiteralUInt, currToken.Kind)
	assert.Equal(t, "0B11001010", currToken.Lexeme)
	assert.Equal(t, 8, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenEOF, currToken.Kind)
}

func TestScannerRecognizesDecimalLiterals(t *testing.T) {
	source := NewSource("123 0815 -456", "")
	scanner, err := NewScanner(source)
	require.NoError(t, err)

	currToken := scanner.Peek()
	require.Equal(t, TokenLiteralUInt, currToken.Kind)
	assert.Equal(t, "123", currToken.Lexeme)
	assert.Equal(t, 1, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenLiteralUInt, currToken.Kind)
	assert.Equal(t, "0815", currToken.Lexeme)
	assert.Equal(t, 5, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenArithMinus, currToken.Kind)
	assert.Equal(t, "-", currToken.Lexeme)
	assert.Equal(t, 10, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenLiteralUInt, currToken.Kind)
	assert.Equal(t, "456", currToken.Lexeme)
	assert.Equal(t, 11, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenEOF, currToken.Kind)
}

func TestScannerRecognizesHexadecimalLiterals(t *testing.T) {
	source := NewSource("0x1f 0X40Bb", "")
	scanner, err := NewScanner(source)
	require.NoError(t, err)

	currToken := scanner.Peek()
	require.Equal(t, TokenLiteralUInt, currToken.Kind)
	assert.Equal(t, "0x1f", currToken.Lexeme)
	assert.Equal(t, 1, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenLiteralUInt, currToken.Kind)
	assert.Equal(t, "0X40Bb", currToken.Lexeme)
	assert.Equal(t, 6, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenEOF, currToken.Kind)
}

//---- Identifiers ------------------------------------------------------------

func TestScannerRecognizesIdentifiers(t *testing.T) {
	source := NewSource("abc Reg1 unit_test _local über", "")
	scanner, err := NewScanner(source)
	require.NoError(t, err)

	currToken := scanner.Peek()
	require.Equal(t, TokenIdentifier, currToken.Kind)
	assert.Equal(t, "abc", currToken.Lexeme)
	assert.Equal(t, 1, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenIdentifier, currToken.Kind)
	assert.Equal(t, "Reg1", currToken.Lexeme)
	assert.Equal(t, 5, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenIdentifier, currToken.Kind)
	assert.Equal(t, "unit_test", currToken.Lexeme)
	assert.Equal(t, 10, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenIdentifier, currToken.Kind)
	assert.Equal(t, "_local", currToken.Lexeme)
	assert.Equal(t, 20, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenIdentifier, currToken.Kind)
	assert.Equal(t, "über", currToken.Lexeme)
	assert.Equal(t, 27, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenEOF, currToken.Kind)
}

//---- Location (line/column) -------------------------------------------------

func TestScannerReturnsTokensWithCorrectLocations(t *testing.T) {
	source := NewSource("abc\n Reg1  über", "")
	scanner, err := NewScanner(source)
	require.NoError(t, err)

	currToken := scanner.Peek()
	require.Equal(t, TokenIdentifier, currToken.Kind)
	assert.Equal(t, "abc", currToken.Lexeme)
	assert.Equal(t, 1, currToken.Location.Column)
	assert.Equal(t, 1, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenIdentifier, currToken.Kind)
	assert.Equal(t, "Reg1", currToken.Lexeme)
	assert.Equal(t, 2, currToken.Location.Column)
	assert.Equal(t, 2, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenIdentifier, currToken.Kind)
	assert.Equal(t, "über", currToken.Lexeme)
	assert.Equal(t, 8, currToken.Location.Column)
	assert.Equal(t, 2, currToken.Location.Line)
	err = scanner.Advance()
	require.NoError(t, err)

	currToken = scanner.Peek()
	require.Equal(t, TokenEOF, currToken.Kind)
}

//---- Operator symbols -------------------------------------------------------

func TestScannerRecognizesOperatorSymbols(t *testing.T) {
	var expectedOperatorTokens = []Token{
		{Lexeme: "/", Kind: TokenArithDivide, Location: Location{Line: 1, Column: 1}},
		{Lexeme: "-", Kind: TokenArithMinus, Location: Location{Line: 1, Column: 3}},
		{Lexeme: "%", Kind: TokenArithModulus, Location: Location{Line: 1, Column: 5}},
		{Lexeme: "*", Kind: TokenArithMultiply, Location: Location{Line: 1, Column: 7}},
		{Lexeme: "+", Kind: TokenArithPlus, Location: Location{Line: 1, Column: 9}},
		{Lexeme: "**", Kind: TokenArithPower, Location: Location{Line: 1, Column: 11}},
		{Lexeme: "<<", Kind: TokenArithShiftLeft, Location: Location{Line: 1, Column: 14}},
		{Lexeme: ">>", Kind: TokenArithShiftRight, Location: Location{Line: 1, Column: 17}},
		{Lexeme: "&", Kind: TokenBitAnd, Location: Location{Line: 1, Column: 20}},
		{Lexeme: "~", Kind: TokenBitNot, Location: Location{Line: 1, Column: 22}},
		{Lexeme: "|", Kind: TokenBitOr, Location: Location{Line: 1, Column: 24}},
		{Lexeme: "^", Kind: TokenBitXOr, Location: Location{Line: 1, Column: 26}},
		{Lexeme: "&&", Kind: TokenBoolAnd, Location: Location{Line: 1, Column: 28}},
		{Lexeme: "!", Kind: TokenBoolNot, Location: Location{Line: 1, Column: 31}},
		{Lexeme: "||", Kind: TokenBoolOr, Location: Location{Line: 1, Column: 33}},
		{Lexeme: "^^", Kind: TokenBoolXOr, Location: Location{Line: 1, Column: 36}},
		{Lexeme: "(", Kind: TokenParenLeft, Location: Location{Line: 1, Column: 39}},
		{Lexeme: ")", Kind: TokenParenRight, Location: Location{Line: 1, Column: 41}},
		{Lexeme: "==", Kind: TokenRelEQ, Location: Location{Line: 1, Column: 43}},
		{Lexeme: ">=", Kind: TokenRelGE, Location: Location{Line: 1, Column: 46}},
		{Lexeme: ">", Kind: TokenRelGT, Location: Location{Line: 1, Column: 49}},
		{Lexeme: "<=", Kind: TokenRelLE, Location: Location{Line: 1, Column: 51}},
		{Lexeme: "<", Kind: TokenRelLT, Location: Location{Line: 1, Column: 54}},
		{Lexeme: "!=", Kind: TokenRelNE, Location: Location{Line: 1, Column: 56}},
		{Lexeme: ":=", Kind: TokenAssignment, Location: Location{Line: 1, Column: 59}},
		{Lexeme: ";", Kind: TokenSemicolon, Location: Location{Line: 1, Column: 62}},
		{Lexeme: ",", Kind: TokenComma, Location: Location{Line: 1, Column: 64}},
	}

	source := NewSource("/ - % * + ** << >> & ~ | ^ && ! || ^^ ( ) == >= > <= < != := ; ,", "")
	scanner, err := NewScanner(source)
	require.NoError(t, err)

	for _, expectedToken := range expectedOperatorTokens {
		actualToken := scanner.Peek()
		require.Equal(t, expectedToken.Kind, actualToken.Kind)
		require.Equal(t, expectedToken.Lexeme, actualToken.Lexeme)
		require.Equal(t, expectedToken.Location.Column, actualToken.Location.Column)
		require.Equal(t, expectedToken.Location.Line, actualToken.Location.Line)
		err = scanner.Advance()
		require.NoError(t, err)
	}

	require.Equal(t, TokenEOF, scanner.Peek().Kind)
}
