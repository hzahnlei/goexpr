package expression

type FunctionApplication struct {
	Base
	Function     Expression
	ArgumentList Expression
}

var _ Expression = (*FunctionApplication)(nil)

func (expr FunctionApplication) Accept(visitor Visitor, ctx VisitorContext) (Expression, error) {
	return visitor.VisitFunctionApplication(expr, ctx)
}

func (expr FunctionApplication) String() string {
	return expr.Function.String() + "(" + expr.ArgumentList.String() + ")"
}
