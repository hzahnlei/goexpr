package expression

import "zahnleiter.org/goexpr/pkg/commons"

type Expression interface {
	// Expressions, parsed from source,  carry the token that founded them.
	// We may, for example, use this token  in error messages, so that mis-
	// takes can be traced back to the right location in the source.
	// Computed expressions, of cause, do not carry a token.  So error mes-
	// sages may lack details.
	HasOriginalSource() bool
	OriginalSource() OptionalOriginalSource
	OriginalRepresentationIfPresent() OptionalRepresentation
	OriginalLocationIfPresent() OptionalOriginalLocation
	IsEOF() bool
	// Visitable
	Accept(Visitor, VisitorContext) (Expression, error)
	// Infix visitable
	IfxAcceptWithUndetermined(InfixVisitor, VisitorContext, Expression) (Expression, error)
	IfxAcceptWithSIntLiteral(InfixVisitor, VisitorContext, SIntLiteral) (Expression, error)
	IfxAcceptWithUIntLiteral(InfixVisitor, VisitorContext, UIntLiteral) (Expression, error)
	IfxAcceptWithFloatLiteral(InfixVisitor, VisitorContext, FloatLiteral) (Expression, error)
	String() string
}

var _ Visitable = (Expression)(nil)
var _ InfixVisitable = (Expression)(nil)

type OriginalSource struct {
	Lexeme   string
	Location OriginalLocation
}

type OriginalLocation struct {
	FileName string
	Line     int
	Column   int
}

type OptionalOriginalSource = commons.Optional[OriginalSource]

type OptionalRepresentation = commons.Optional[string]

type OptionalOriginalLocation = commons.Optional[OriginalLocation]
