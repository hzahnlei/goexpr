package expression

type Parenthesis struct {
	Base
	Expression Expression
	Origin     OptionalOriginalSource
}

var _ Expression = (*Parenthesis)(nil)

func (expr Parenthesis) Accept(visitor Visitor, ctx VisitorContext) (Expression, error) {
	return visitor.VisitParenthesis(expr, ctx)
}

func (expr Parenthesis) String() string {
	return "(" + expr.Expression.String() + ")"
}
