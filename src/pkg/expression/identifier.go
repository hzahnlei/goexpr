package expression

type Identifier struct {
	Base
	Name string
}

var _ Expression = (*Identifier)(nil)

func (expr Identifier) Accept(visitor Visitor, ctx VisitorContext) (Expression, error) {
	return visitor.VisitIdentifier(expr, ctx)

}

// Called in case an identifier participates as the LHS of an infix expression.
// This is mainly the assignement operator := but might be further operators in
// the future. Example: <ident> := <sint>
func (expr Identifier) IfxAcceptWithSIntLiteral(visitor InfixVisitor, ctx VisitorContext, lhs SIntLiteral) (Expression, error) {
	return visitor.IfxVisitSIntAndIdentifier(lhs, expr, ctx)
}

// Called in case an identifier participates as the LHS of an infix expression.
// This is mainly the assignement operator := but might be further operators in
// the future. Example: <ident> := <uint>
func (expr Identifier) IfxAcceptWithUIntLiteral(visitor InfixVisitor, ctx VisitorContext, lhs UIntLiteral) (Expression, error) {
	return visitor.IfxVisitUIntAndIdentifier(lhs, expr, ctx)
}

// Called in case an identifier participates as the LHS of an infix expression.
// This is mainly the assignement operator := but might be further operators in
// the future. Example: <ident> := <float>
func (expr Identifier) IfxAcceptWithFloatLiteral(visitor InfixVisitor, ctx VisitorContext, lhs FloatLiteral) (Expression, error) {
	return visitor.IfxVisitFloatAndIdentifier(lhs, expr, ctx)
}

func (expr Identifier) String() string {
	return expr.Name
}
