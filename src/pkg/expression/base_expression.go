package expression

import "zahnleiter.org/goexpr/pkg/commons"

type Base struct {
	Origin OptionalOriginalSource
}

func (expr Base) HasOriginalSource() bool {
	return expr.Origin.HasValue()
}

func (expr Base) OriginalSource() OptionalOriginalSource {
	return expr.Origin
}

func (expr Base) OriginalRepresentationIfPresent() OptionalRepresentation {
	if expr.Origin.HasValue() {
		return commons.OptionalOf(expr.Origin.Value().Lexeme)
	}
	return OptionalRepresentation{}
}

func (expr Base) OriginalLocationIfPresent() OptionalOriginalLocation {
	if expr.Origin.HasValue() {
		return commons.OptionalOf(expr.Origin.Value().Location)
	}
	return OptionalOriginalLocation{}
}

func (expr Base) IsEOF() bool {
	return false
}

// Override if required (expression participates as LHS or RHS in infix expression)
func (expr Base) IfxAcceptWithUndetermined(InfixVisitor, VisitorContext, Expression) (Expression, error) {
	panic(infixVisitorMustNotBeApplied) // Should never happen
}

// Override if required (expression participates as LHS or RHS in infix expression)
func (expr Base) IfxAcceptWithSIntLiteral(InfixVisitor, VisitorContext, SIntLiteral) (Expression, error) {
	panic(infixVisitorMustNotBeApplied) // Should never happen
}

// Override if required (expression participates as LHS or RHS in infix expression)
func (expr Base) IfxAcceptWithUIntLiteral(InfixVisitor, VisitorContext, UIntLiteral) (Expression, error) {
	panic(infixVisitorMustNotBeApplied) // Should never happen
}

func (expr Base) IfxAcceptWithFloatLiteral(InfixVisitor, VisitorContext, FloatLiteral) (Expression, error) {
	panic(infixVisitorMustNotBeApplied) // Should never happen
}

const infixVisitorMustNotBeApplied = "Infix visitor must not be applied to expression of this type."
