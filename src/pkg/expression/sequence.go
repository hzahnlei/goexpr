package expression

import "strings"

type Sequence struct {
	Base
	Elements []Expression
}

var _ Expression = (*Sequence)(nil)

func (expr Sequence) Accept(visitor Visitor, ctx VisitorContext) (Expression, error) {
	return visitor.VisitSequence(expr, ctx)
}

func (expr Sequence) String() string {
	var result strings.Builder
	for i, element := range expr.Elements {
		if i > 0 {
			result.WriteString("; ")
		}
		result.WriteString(element.String())
	}
	return result.String()
}
