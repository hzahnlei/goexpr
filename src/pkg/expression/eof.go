package expression

type EOF struct {
	Base
}

var _ Expression = (*EOF)(nil)

func (expr EOF) Accept(visitor Visitor, ctx VisitorContext) (Expression, error) {
	return visitor.VisitEOF(expr, ctx)
}

func (expr EOF) IsEOF() bool {
	return true
}

func (expr EOF) String() string {
	return "/* EOF */"
}
