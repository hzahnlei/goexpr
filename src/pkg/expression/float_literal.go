package expression

import (
	"fmt"
)

type FloatLiteral struct {
	Base
	Value float64
}

var _ Expression = (*FloatLiteral)(nil)

func (expr FloatLiteral) IsZero() bool {
	return expr.Value == 0
}

func (expr FloatLiteral) Accept(visitor Visitor, ctx VisitorContext) (Expression, error) {
	return visitor.VisitFloatLiteral(expr, ctx)
}

func (lhs FloatLiteral) IfxAcceptWithUndetermined(visitor InfixVisitor, ctx VisitorContext, rhs Expression) (Expression, error) {
	return visitor.IfxVisitFloatWithUndetermined(lhs, rhs, ctx)
}

// Note how rhs and lhs are getting changed here!
func (rhs FloatLiteral) IfxAcceptWithSIntLiteral(visitor InfixVisitor, ctx VisitorContext, lhs SIntLiteral) (Expression, error) {
	return visitor.IfxVisitSIntAndFloat(lhs, rhs, ctx)
}

// Note how rhs and lhs are getting changed here!
func (rhs FloatLiteral) IfxAcceptWithUIntLiteral(visitor InfixVisitor, ctx VisitorContext, lhs UIntLiteral) (Expression, error) {
	return visitor.IfxVisitUIntAndFloat(lhs, rhs, ctx)
}

// Note how rhs and lhs are getting changed here!
func (rhs FloatLiteral) IfxAcceptWithFloatLiteral(visitor InfixVisitor, ctx VisitorContext, lhs FloatLiteral) (Expression, error) {
	return visitor.IfxVisitFloatAndFloat(lhs, rhs, ctx)
}

func (expr FloatLiteral) String() string {
	return fmt.Sprintf("%f", expr.Value)
}
