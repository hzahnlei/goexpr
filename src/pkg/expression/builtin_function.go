package expression

type BuiltinFunction struct {
	Base
	Name string
	F    func(Expression) (Expression, error)
}

var _ Expression = (*BuiltinFunction)(nil)

func (expr BuiltinFunction) Accept(visitor Visitor, ctx VisitorContext) (Expression, error) {
	return visitor.VisitBuiltinFunction(expr, ctx)
}

func (expr BuiltinFunction) IfxAcceptWithUndetermined(eval InfixVisitor, ctx VisitorContext, args Expression) (Expression, error) {
	// Brute force
	return expr.F(args)
}

func (expr BuiltinFunction) String() string {
	return expr.Name
}
