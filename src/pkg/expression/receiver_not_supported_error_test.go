package expression

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestReceiverNotSupported(t *testing.T) {
	// GIVEN
	funName := "count"
	visitorOfAnyType := t
	// WHEN
	actualMessage := ReceiverNotSupported(funName, visitorOfAnyType)
	// THEN
	require.Equal(t, `Receiver function "count" not supported by type *testing.T.`, actualMessage)
}
