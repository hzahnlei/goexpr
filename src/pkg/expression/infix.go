package expression

import "fmt"

type Infix struct {
	Base
	OperatorType InfixOperatorType
	LHSArgument  Expression
	RHSArgument  Expression
}

var _ Expression = (*Infix)(nil)

func (expr Infix) Accept(visitor Visitor, ctx VisitorContext) (Expression, error) {
	return visitor.VisitInfixExpression(expr, ctx)
}

func (expr Infix) String() string {
	return expr.LHSArgument.String() + " " + expr.OperatorType.String() + " " + expr.RHSArgument.String()
}

// ---- Infix operators -------------------------------------------------------

type InfixOperatorType int

const (
	InfixOpArithDivide     InfixOperatorType = iota // /
	InfixOpArithMinus                               // -
	InfixOpArithModulus                             // %
	InfixOpArithMultiply                            // *
	InfixOpArithPower                               // **
	InfixOpArithPlus                                // +
	InfixOpArithShiftLeft                           // <<
	InfixOpArithShiftRight                          // >>
	InfixOpAssignment                               // :=
	InfixOpBitAnd                                   // &
	InfixOpBitOr                                    // |
	InfixOpBitXOr                                   // ^
	InfixOpBoolAnd                                  // &&
	InfixOpBoolOr                                   // ||
	InfixOpBoolXOr                                  // ^^
	InfixOpRelEQ                                    // ==
	InfixOpRelGE                                    // >=
	InfixOpRelGT                                    // >
	InfixOpRelLE                                    // <=
	InfixOpRelLT                                    // <
	InfixOpRelNE                                    // !=
)

func (opType InfixOperatorType) String() string {
	if str, found := map[InfixOperatorType]string{
		InfixOpArithDivide:     "/",
		InfixOpArithMinus:      "-",
		InfixOpArithModulus:    "%",
		InfixOpArithMultiply:   "*",
		InfixOpArithPlus:       "+",
		InfixOpArithPower:      "**",
		InfixOpArithShiftLeft:  "<<",
		InfixOpArithShiftRight: ">>",
		InfixOpBitAnd:          "&",
		InfixOpBitOr:           "|",
		InfixOpBitXOr:          "^",
		InfixOpBoolAnd:         "&&",
		InfixOpBoolOr:          "||",
		InfixOpBoolXOr:         "^^",
		InfixOpRelEQ:           "==",
		InfixOpRelGE:           ">=",
		InfixOpRelGT:           ">",
		InfixOpRelLE:           "<=",
		InfixOpRelLT:           "<",
		InfixOpRelNE:           "!=",
	}[opType]; found {
		return str
	}
	// May only happen in case set of operators gets extended/reduced but
	// this function did not get adapted accordingly.
	panic(fmt.Sprintf("Unknown operator %d", opType))
}
