package expression

import (
	"fmt"

	"zahnleiter.org/goexpr/pkg/commons"
)

// Integer literals parsed from the input have an original text representation,
// for example "0x6A",  "123",  or "0b11001010".  In this case we might want to
// print that original expression  so that the user can more easily relate that
// output to his/her input.
// Computed integers of cause habe no original text representation.  If this is
// the case,  then we have no choice but to print the decimal representation of
// the integer value.
type SIntLiteral struct {
	Base
	Value any
}

var _ Expression = (*SIntLiteral)(nil)

// Use this constructors as it noly allows to create valid signed integer lite-
// rals (with supported base types).
// The evaluator will use this constructor as computed expressions have no ori-
// gin in source code that could help us in constructing better error messages.
func NewSInt[T SIntTypes](value T) SIntLiteral {
	return SIntLiteral{
		Base:  Base{},
		Value: value,
	}
}

type SIntTypes interface {
	int8 | int16 | int32 | int64
}

// Use this constructors as it noly allows to create valid signed integer lite-
// rals (with supported base types).
// The parser  will use  this constructor.  It adds a link to the  source code,
// which we might use later on to produce better error messages.
func NewSIntWithOrigin[T SIntTypes](value T, origin OptionalOriginalSource) SIntLiteral {
	return SIntLiteral{
		Base: Base{
			Origin: origin,
		},
		Value: value,
	}
}

func (expr SIntLiteral) IsZero() bool {
	switch value := expr.Value.(type) {
	case int8:
		return value == 0
	case int16:
		return value == 0
	case int32:
		return value == 0
	case int64:
		return value == 0
	default:
		panic(commons.NotASupportedSInt(expr)) // Should never happen
	}
}

func (expr SIntLiteral) Accept(visitor Visitor, ctx VisitorContext) (Expression, error) {
	return visitor.VisitSIntLiteral(expr, ctx)
}

func (lhs SIntLiteral) IfxAcceptWithUndetermined(visitor InfixVisitor, ctx VisitorContext, rhs Expression) (Expression, error) {
	return visitor.IfxVisitSIntWithUndetermined(lhs, rhs, ctx)
}

// Note how rhs and lhs are getting changed here!
func (rhs SIntLiteral) IfxAcceptWithSIntLiteral(visitor InfixVisitor, ctx VisitorContext, lhs SIntLiteral) (Expression, error) {
	return visitor.IfxVisitSIntAndSInt(lhs, rhs, ctx)
}

// Note how rhs and lhs are getting changed here!
func (rhs SIntLiteral) IfxAcceptWithUIntLiteral(visitor InfixVisitor, ctx VisitorContext, lhs UIntLiteral) (Expression, error) {
	return visitor.IfxVisitUIntAndSInt(lhs, rhs, ctx)
}

func (expr SIntLiteral) String() string {
	switch value := expr.Value.(type) {
	case int8:
		return fmt.Sprintf("%ds8", value)
	case int16:
		return fmt.Sprintf("%ds16", value)
	case int32:
		return fmt.Sprintf("%ds32", value)
	default:
		return fmt.Sprintf("%ds64", value)
	}
}
