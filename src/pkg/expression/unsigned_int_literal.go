package expression

import (
	"fmt"

	"zahnleiter.org/goexpr/pkg/commons"
)

// Integer literals parsed from the input have an original text representation,
// for example "0x6A", "12345", or "0b11001010".  In this case we might want to
// use that original representation, for example in error messages, so that the
// user can more easily relate that output to his/her input.
// Computed integers of cause have no original text representation.  If this is
// the case,  then we have no choice but to print the decimal representation of
// the integer value.
type UIntLiteral struct {
	Base
	Value any
}

// Use this constructors as it noly allows to create valid unsigned integer li-
// terals (with supported base types).
// The evaluator will use this constructor as computed expressions have no ori-
// gin in source code that could help us in constructing better error messages.
func NewUInt[T UIntTypes](value T) UIntLiteral {
	return UIntLiteral{
		Base:  Base{},
		Value: value,
	}
}

type UIntTypes interface {
	uint8 | uint16 | uint32 | uint64
}

// Use this constructors as it noly allows to create valid unsigned integer li-
// terals (with supported base types).
// The parser  will use  this constructor.  It adds a link to the  source code,
// which we might use later on to produce better error messages.
func NewUIntWithOrigin[T UIntTypes](value T, origin OptionalOriginalSource) UIntLiteral {
	return UIntLiteral{
		Base: Base{
			Origin: origin,
		},
		Value: value,
	}
}

func (expr UIntLiteral) IsZero() bool {
	switch value := expr.Value.(type) {
	case uint8:
		return value == 0
	case uint16:
		return value == 0
	case uint32:
		return value == 0
	case uint64:
		return value == 0
	default:
		panic(commons.NotASupportedUInt(expr)) // Should never happen
	}
}

func (expr UIntLiteral) Accept(visitor Visitor, ctx VisitorContext) (Expression, error) {
	return visitor.VisitUIntLiteral(expr, ctx)
}

func (lhs UIntLiteral) IfxAcceptWithUndetermined(visitor InfixVisitor, ctx VisitorContext, rhs Expression) (Expression, error) {
	return visitor.IfxVisitUIntWithUndetermined(lhs, rhs, ctx)
}

// Note how rhs and lhs are getting changed here!
func (rhs UIntLiteral) IfxAcceptWithSIntLiteral(visitor InfixVisitor, ctx VisitorContext, lhs SIntLiteral) (Expression, error) {
	return visitor.IfxVisitSIntAndUInt(lhs, rhs, ctx)
}

// Note how rhs and lhs are getting changed here!
func (rhs UIntLiteral) IfxAcceptWithUIntLiteral(visitor InfixVisitor, ctx VisitorContext, lhs UIntLiteral) (Expression, error) {

	return visitor.IfxVisitUIntAndUInt(lhs, rhs, ctx)
}

func (expr UIntLiteral) String() string {
	switch value := expr.Value.(type) {
	case uint8:
		return fmt.Sprintf("%du8", value)
	case uint16:
		return fmt.Sprintf("%du16", value)
	case uint32:
		return fmt.Sprintf("%du32", value)
	default:
		return fmt.Sprintf("%du64", value)
	}
}
