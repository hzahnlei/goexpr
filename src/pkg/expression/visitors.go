package expression

//---- "Normal" visitor pattern -----------------------------------------------

type Visitable interface {
	Accept(Visitor, VisitorContext) (Expression, error)
}

// Resolves the generic data type Expression to one of the concrete data types,
// for example EOFExpression, IdentifierExpression, etc.
type Visitor interface {
	VisitEOF(EOF, VisitorContext) (Expression, error)
	VisitIdentifier(Identifier, VisitorContext) (Expression, error)
	VisitSIntLiteral(SIntLiteral, VisitorContext) (Expression, error)
	VisitUIntLiteral(UIntLiteral, VisitorContext) (Expression, error)
	VisitParenthesis(Parenthesis, VisitorContext) (Expression, error)
	VisitInfixExpression(Infix, VisitorContext) (Expression, error)
	VisitPrefixExpression(Prefix, VisitorContext) (Expression, error)
	VisitSequence(Sequence, VisitorContext) (Expression, error)
	VisitList(List, VisitorContext) (Expression, error)
	VisitFunctionApplication(FunctionApplication, VisitorContext) (Expression, error)
	VisitBuiltinFunction(BuiltinFunction, VisitorContext) (Expression, error)
	VisitFloatLiteral(FloatLiteral, VisitorContext) (Expression, error)
}

type VisitorContext interface {
	IsDefined(key string) bool
	IsDefinedLocally(key string) bool
	Lookup(key string) (Expression, bool)
	Define(key string, value Expression) error
}

//---- Infix visitor pattern --------------------------------------------------

type InfixVisitable interface {
	IfxAcceptWithUndetermined(InfixVisitor, VisitorContext, Expression) (Expression, error)

	IfxAcceptWithSIntLiteral(InfixVisitor, VisitorContext, SIntLiteral) (Expression, error)

	IfxAcceptWithUIntLiteral(InfixVisitor, VisitorContext, UIntLiteral) (Expression, error)

	IfxAcceptWithFloatLiteral(InfixVisitor, VisitorContext, FloatLiteral) (Expression, error)
}

// Resolves the generic data type Expression to one of the concrete data types,
// just like the commonly known visitor pattern. In opposite to the well known,
// this  visitor  resolves the types of  two expressions.  This is particularly
// handy when  evaluating infix expressions.  In such a case, one needs to know
// the types of both operands. For example: Multiplying two integers works.  On
// the other hand, multiplying a string and an integer, may make no sense.  (At
// least it would be unintuitive.)
type InfixVisitor interface {
	IfxVisitSIntWithUndetermined(SIntLiteral, Expression, VisitorContext) (Expression, error)
	IfxVisitSIntAndSInt(SIntLiteral, SIntLiteral, VisitorContext) (Expression, error)
	IfxVisitSIntAndUInt(SIntLiteral, UIntLiteral, VisitorContext) (Expression, error)
	IfxVisitSIntAndFloat(SIntLiteral, FloatLiteral, VisitorContext) (Expression, error)
	// Used by assignment: <ident> := <sint>
	IfxVisitSIntAndIdentifier(SIntLiteral, Identifier, VisitorContext) (Expression, error)

	IfxVisitUIntWithUndetermined(UIntLiteral, Expression, VisitorContext) (Expression, error)
	IfxVisitUIntAndSInt(UIntLiteral, SIntLiteral, VisitorContext) (Expression, error)
	IfxVisitUIntAndUInt(UIntLiteral, UIntLiteral, VisitorContext) (Expression, error)
	IfxVisitUIntAndFloat(UIntLiteral, FloatLiteral, VisitorContext) (Expression, error)
	// Used by assignment: <ident> := <uint>
	IfxVisitUIntAndIdentifier(UIntLiteral, Identifier, VisitorContext) (Expression, error)

	IfxVisitFloatWithUndetermined(FloatLiteral, Expression, VisitorContext) (Expression, error)
	IfxVisitFloatAndSInt(FloatLiteral, SIntLiteral, VisitorContext) (Expression, error)
	IfxVisitFloatAndUInt(FloatLiteral, UIntLiteral, VisitorContext) (Expression, error)
	IfxVisitFloatAndFloat(FloatLiteral, FloatLiteral, VisitorContext) (Expression, error)
	// Used by assignment: <ident> := <float>
	IfxVisitFloatAndIdentifier(FloatLiteral, Identifier, VisitorContext) (Expression, error)
}
