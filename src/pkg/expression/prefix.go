package expression

import "fmt"

type Prefix struct {
	Base
	OperatorType PrefixOperatorType
	Argument     Expression
}

var _ Expression = (*Prefix)(nil)

func (expr Prefix) Accept(visitor Visitor, ctx VisitorContext) (Expression, error) {
	return visitor.VisitPrefixExpression(expr, ctx)
}

func (expr Prefix) String() string {
	return expr.OperatorType.String() + expr.Argument.String()
}

// ---- Prefix operators ------------------------------------------------------

type PrefixOperatorType int

const (
	PrefixOpArithNegate   PrefixOperatorType = iota // -
	PrefixOpBitComplement                           // ~
	PrefixOpBoolNot                                 // !
)

func (opType PrefixOperatorType) String() string {
	if str, found := map[PrefixOperatorType]string{
		PrefixOpArithNegate:   "-",
		PrefixOpBitComplement: "~",
		PrefixOpBoolNot:       "!",
	}[opType]; found {
		return str
	}
	// May only happen in case set of operators gets extended/reduced but
	// this function did not get adapted accordingly.
	panic(fmt.Sprintf("Unknown operator %d", opType))
}
