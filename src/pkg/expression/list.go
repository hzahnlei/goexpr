package expression

type List struct {
	Base
	OperatorType InfixOperatorType
	Head         Expression
	Tail         Expression
}

var _ Expression = (*List)(nil)

func (expr List) Accept(visitor Visitor, ctx VisitorContext) (Expression, error) {
	return visitor.VisitList(expr, ctx)
}

func (expr List) String() string {
	return expr.Head.String() + ", " + expr.Tail.String()
}
