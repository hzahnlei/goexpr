package expression

type BaseVisitor struct{}

func (v BaseVisitor) VisitEOF(EOF, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("VisitEOF", v))
}
func (v BaseVisitor) VisitIdentifier(Identifier, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("VisitIdentifier", v))
}
func (v BaseVisitor) VisitSIntLiteral(SIntLiteral, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("VisitSIntLiteral", v))
}
func (v BaseVisitor) VisitUIntLiteral(UIntLiteral, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("VisitUIntLiteral", v))
}
func (v BaseVisitor) VisitParenthesis(Parenthesis, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("VisitParenthesis", v))
}
func (v BaseVisitor) VisitInfixExpression(Infix, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("VisitInfixExpression", v))
}
func (v BaseVisitor) VisitPrefixExpression(Prefix, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("VisitPrefixExpression", v))
}
func (v BaseVisitor) VisitSequence(Sequence, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("VisitSequence", v))
}
func (v BaseVisitor) VisitList(List, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("VisitList", v))
}
func (v BaseVisitor) VisitFunctionApplication(FunctionApplication, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("VisitFunctionApplication", v))
}
func (v BaseVisitor) VisitBuiltinFunction(BuiltinFunction, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("VisitBuiltinFunction", v))
}
func (v BaseVisitor) VisitFloatLiteral(FloatLiteral, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("VisitFloatLiteral", v))
}

type BaseInfixVisitor struct{}

func (v BaseInfixVisitor) IfxVisitSIntWithUndetermined(SIntLiteral, Expression, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitSIntWithUndetermined", v))
}
func (v BaseInfixVisitor) IfxVisitSIntAndSInt(SIntLiteral, SIntLiteral, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitSIntAndSInt", v))
}
func (v BaseInfixVisitor) IfxVisitSIntAndUInt(SIntLiteral, UIntLiteral, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitSIntAndUInt", v))
}
func (v BaseInfixVisitor) IfxVisitSIntAndFloat(SIntLiteral, FloatLiteral, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitSIntAndFloat", v))
}
func (v BaseInfixVisitor) IfxVisitSIntAndIdentifier(SIntLiteral, Identifier, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitSIntAndIdentifier", v))
}

func (v BaseInfixVisitor) IfxVisitUIntWithUndetermined(UIntLiteral, Expression, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitUIntWithUndetermined", v))
}
func (v BaseInfixVisitor) IfxVisitUIntAndSInt(UIntLiteral, SIntLiteral, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitUIntAndSInt", v))
}
func (v BaseInfixVisitor) IfxVisitUIntAndUInt(UIntLiteral, UIntLiteral, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitUIntAndUInt", v))
}
func (v BaseInfixVisitor) IfxVisitUIntAndFloat(UIntLiteral, FloatLiteral, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitUIntAndFloat", v))
}
func (v BaseInfixVisitor) IfxVisitUIntAndIdentifier(UIntLiteral, Identifier, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitUIntAndIdentifier", v))
}

func (v BaseInfixVisitor) IfxVisitFloatWithUndetermined(FloatLiteral, Expression, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitFloatWithUndetermined", v))
}
func (v BaseInfixVisitor) IfxVisitFloatAndSInt(FloatLiteral, SIntLiteral, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitFloatAndSInt", v))
}
func (v BaseInfixVisitor) IfxVisitFloatAndUInt(FloatLiteral, UIntLiteral, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitFloatAndUInt", v))
}
func (v BaseInfixVisitor) IfxVisitFloatAndFloat(FloatLiteral, FloatLiteral, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitFloatAndFloat", v))
}
func (v BaseInfixVisitor) IfxVisitFloatAndIdentifier(FloatLiteral, Identifier, VisitorContext) (Expression, error) {
	panic(ReceiverNotSupported("IfxVisitFloatAndIdentifier", v))
}
