package expression

import "fmt"

// A convenience function for the expressions base class. The base class imple-
// ments all visitor functions by  issuing this error message.  Derived classes
// may decide to override only thos receivers from the base which are required.
// If this error message appears:
//   - Check if the  derived class  has to implement the corresponding receiver
//     method.
//   - If this is not the case,  then check why that receiver  has been invoked
//     anyways.
//
// This error message shoud never occur, if this library is properly tested and
// used as intended.
func ReceiverNotSupported(receiverFunName string, typeName any) string {
	return fmt.Sprintf("Receiver function %q not supported by type %T.", receiverFunName, typeName)
}
