package goexpr_test

import (
	"testing"

	"github.com/stretchr/testify/require"
)

// ---- SPEP test = scan, parse, eval print test ------------------------------

func TestSPEPOperatorPrecedenceTest(t *testing.T) {
	// GIVEN
	source := "3 + 4 * 5"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "23", result)
}

func TestSPEPOperatorPrecedenceWithParenthesisTest(t *testing.T) {
	// GIVEN
	source := "(3 + 4) * 5"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "35", result)
}

func TestSPEPOperatorPrecedenceWithRelationalOperatorsTest(t *testing.T) {
	// GIVEN
	source := "3 + 4 * 5 < (3 + 4) * 5"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "1", result)
}
