package eval

import (
	"fmt"

	"zahnleiter.org/goexpr/pkg/expression"
)

// The top-level expression evaluator. The visitor pattern is used to implement
// the evaluation of expressions.  This evaluator uses another evaluator,  also
// based on the visitor pattern, to evaluate sub-expressions that are infix ex-
// pressions.
type topLevelEvaluator struct {
	expression.BaseVisitor
}

var _ Evaluator = (*topLevelEvaluator)(nil)

// Default evaluation function using the visitor pattern under the hood.
func Evaluate(expr expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	evaluator := newTopLevelEvaluator()
	return expr.Accept(evaluator, ctx)
}

func newTopLevelEvaluator() topLevelEvaluator {
	return topLevelEvaluator{}
}

func (eval topLevelEvaluator) evaluate(expr expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	return expr.Accept(eval, ctx)
}

func (eval topLevelEvaluator) VisitEOF(expr expression.EOF, ctx expression.VisitorContext) (expression.Expression, error) {
	return expr, nil
}

func (eval topLevelEvaluator) VisitIdentifier(expr expression.Identifier, ctx expression.VisitorContext) (expression.Expression, error) {
	if expr, found := ctx.Lookup(expr.Name); found {
		return expr, nil
	}
	return nil, runtimeError(fmt.Sprintf("Identifier %q is not defined.", expr.Name), expr.OriginalLocationIfPresent())
}

func (eval topLevelEvaluator) VisitSIntLiteral(expr expression.SIntLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	return expr, nil
}

func (eval topLevelEvaluator) VisitUIntLiteral(expr expression.UIntLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	return expr, nil
}

func (eval topLevelEvaluator) VisitParenthesis(expr expression.Parenthesis, ctx expression.VisitorContext) (expression.Expression, error) {
	return eval.evaluate(expr.Expression, ctx)
}

func (eval topLevelEvaluator) VisitInfixExpression(expr expression.Infix, ctx expression.VisitorContext) (expression.Expression, error) {
	if isAssignment(expr.OperatorType) {
		eval := newAssigmentEvaluator(expr)
		return eval.evaluate(expr.LHSArgument, expr.RHSArgument, ctx)
	} else if canBeShortCircuited(expr.OperatorType) {
		eval := newShortCircuitInfixEvaluator(expr.OperatorType, expr.OriginalSource())
		return eval.evaluate(expr.LHSArgument, expr.RHSArgument, ctx)
	} else {
		eval := newNonShortCircuitInfixEvaluator(expr.OperatorType, expr.OriginalSource())
		return eval.evaluate(expr.LHSArgument, expr.RHSArgument, ctx)
	}
}

func isAssignment(operatorType expression.InfixOperatorType) bool {
	return operatorType == expression.InfixOpAssignment
}

func canBeShortCircuited(op expression.InfixOperatorType) bool {
	switch op {
	case
		expression.InfixOpArithDivide, expression.InfixOpBoolOr, expression.InfixOpBoolAnd:
		return true
	default:
		return false
	}
}

func (eval topLevelEvaluator) VisitPrefixExpression(expr expression.Prefix, ctx expression.VisitorContext) (expression.Expression, error) {
	prefixEval := newPrefixEvaluator(expr.OperatorType, expr.OriginalSource())
	return prefixEval.evaluate(expr.Argument, ctx)
}

// We create a local scope (aka stack, aka context) for the sequence. This sco-
// pe applies to the  complete sequence.  Definitions done within a sequence do
// not leak to the outside th the sequence.  The last expression determines the
// result of the expression.
func (eval topLevelEvaluator) VisitSequence(expr expression.Sequence, ctx expression.VisitorContext) (expression.Expression, error) {
	localCtx := NewMapBasedVisitorContext(ctx)
	var result expression.Expression = expression.EOF{}
	var err error = nil
	for i, element := range expr.Elements {
		result, err = eval.evaluate(element, &localCtx)
		if err != nil {
			// TODO wrapping errors to provide more context.
			// Could include expression as available at this stage of evaluation.
			// Requires new error struct to hold context information
			// Could print well formatted (indented) error message
			return nil, fmt.Errorf("evaluating sequence element #%d: %w", i, err)
		}
	}
	return result, nil
}

func (eval topLevelEvaluator) VisitList(expr expression.List, ctx expression.VisitorContext) (expression.Expression, error) {
	head, err := eval.evaluate(expr.Head, ctx)
	if err != nil {
		return nil, err
	}
	if expr.Tail == nil {
		return expression.List{Head: head, Tail: nil}, nil
	}
	tail, err := eval.evaluate(expr.Tail, ctx)
	if err != nil {
		return nil, err
	}
	return expression.List{Head: head, Tail: tail}, nil
}

func (eval topLevelEvaluator) VisitFunctionApplication(expr expression.FunctionApplication, ctx expression.VisitorContext) (expression.Expression, error) {
	funEval := newFunctionEvaluator(expr.OriginalSource())
	return funEval.evaluate(expr.Function, expr.ArgumentList, ctx)
}

func (eval topLevelEvaluator) VisitFloatLiteral(expr expression.FloatLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	return expr, nil
}
