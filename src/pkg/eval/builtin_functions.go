package eval

import (
	"fmt"
	"math"

	"zahnleiter.org/goexpr/pkg/expression"
)

func RegisterBuiltinFunctions(ctx expression.VisitorContext) {
	// ---- Type casting ----
	mustDefine(ctx, newCastToUIntFunction[uint8]("u8"))
	mustDefine(ctx, newCastToUIntFunction[uint16]("u16"))
	mustDefine(ctx, newCastToUIntFunction[uint32]("u32"))
	mustDefine(ctx, newCastToUIntFunction[uint64]("u64"))

	mustDefine(ctx, newCastToSIntFunction[int8]("s8"))
	mustDefine(ctx, newCastToSIntFunction[int16]("s16"))
	mustDefine(ctx, newCastToSIntFunction[int32]("s32"))
	mustDefine(ctx, newCastToSIntFunction[int64]("s64"))

	mustDefine(ctx, newCastToFloatFunction("f64"))
	// ---- Float functions ----
	mustDefine(ctx, newAbsFunction("abs"))
	mustDefine(ctx, newCeilFunction("ceil"))
	mustDefine(ctx, newCosFunction("cos"))
	mustDefine(ctx, newFloorFunction("floor"))
	mustDefine(ctx, newLbFunction("lb"))
	mustDefine(ctx, newLgFunction("lg"))
	mustDefine(ctx, newLnFunction("ln"))
	mustDefine(ctx, newRoundFunction("round"))
	mustDefine(ctx, newSgnFunction("sgn"))
	mustDefine(ctx, newSinFunction("sin"))
	mustDefine(ctx, newSqFunction("sq"))
	mustDefine(ctx, newSqrtFunction("sqrt"))
	mustDefine(ctx, newTanFunction("tan"))

}

func mustDefine(ctx expression.VisitorContext, fun *expression.BuiltinFunction) {
	if err := ctx.Define(fun.Name, fun); err != nil {
		panic(fmt.Sprintf("Registering built-in function: %s", err.Error()))
	}
}

func newCastToUIntFunction[T UIntTypes](name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.SIntLiteral:
				switch val := valExpr.Value.(type) {
				case int8:
					return expression.NewUInt(T(val)), nil
				case int16:
					return expression.NewUInt(T(val)), nil
				case int32:
					return expression.NewUInt(T(val)), nil
				case int64:
					return expression.NewUInt(T(val)), nil
				}
			case expression.UIntLiteral:
				switch val := valExpr.Value.(type) {
				case uint8:
					return expression.NewUInt(T(val)), nil
				case uint16:
					return expression.NewUInt(T(val)), nil
				case uint32:
					return expression.NewUInt(T(val)), nil
				case uint64:
					return expression.NewUInt(T(val)), nil
				}
			}
			return nil, runtimeError(fmt.Sprintf("Cannot cast %s to %s", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newCastToSIntFunction[T SIntTypes](name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.SIntLiteral:
				switch val := valExpr.Value.(type) {
				case int8:
					return expression.NewSInt(val), nil
				case int16:
					return expression.NewSInt(val), nil
				case int32:
					return expression.NewSInt(val), nil
				case int64:
					return expression.NewSInt(val), nil
				}
			case expression.UIntLiteral:
				switch val := valExpr.Value.(type) {
				case uint8:
					return expression.NewSInt(int8(val)), nil
				case uint16:
					return expression.NewSInt(int16(val)), nil
				case uint32:
					return expression.NewSInt(int32(val)), nil
				case uint64:
					return expression.NewSInt(int64(val)), nil
				}
			}
			return nil, runtimeError(fmt.Sprintf("Cannot cast %s to %s (neitehr a signed nor an unsigned integer number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newCastToFloatFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.SIntLiteral:
				switch val := valExpr.Value.(type) {
				case int8:
					return expression.FloatLiteral{Value: float64(val)}, nil
				case int16:
					return expression.FloatLiteral{Value: float64(val)}, nil
				case int32:
					return expression.FloatLiteral{Value: float64(val)}, nil
				case int64:
					return expression.FloatLiteral{Value: float64(val)}, nil
				}
			case expression.UIntLiteral:
				switch val := valExpr.Value.(type) {
				case uint8:
					return expression.FloatLiteral{Value: float64(val)}, nil
				case uint16:
					return expression.FloatLiteral{Value: float64(val)}, nil
				case uint32:
					return expression.FloatLiteral{Value: float64(val)}, nil
				case uint64:
					return expression.FloatLiteral{Value: float64(val)}, nil
				}
			case expression.FloatLiteral:
				return expr, nil
			}
			return nil, runtimeError(fmt.Sprintf("Cannot cast %s to %s (neither a signed/unsigned integer nor a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newAbsFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.FloatLiteral:
				return expression.FloatLiteral{Value: math.Abs(valExpr.Value)}, nil
			}
			return nil, runtimeError(fmt.Sprintf("Cannot apply %s to %s (not a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newCeilFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.FloatLiteral:
				return expression.FloatLiteral{Value: math.Ceil(valExpr.Value)}, nil
			}
			return nil, runtimeError(fmt.Sprintf("Cannot apply %s to %s (not a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newCosFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.FloatLiteral:
				return expression.FloatLiteral{Value: math.Cos(valExpr.Value)}, nil
			}
			return nil, runtimeError(fmt.Sprintf("Cannot apply %s to %s (not a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newFloorFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.FloatLiteral:
				return expression.FloatLiteral{Value: math.Floor(valExpr.Value)}, nil
			}
			return nil, runtimeError(fmt.Sprintf("Cannot apply %s to %s (not a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newLbFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.FloatLiteral:
				return expression.FloatLiteral{Value: math.Log2(valExpr.Value)}, nil
			}
			return nil, runtimeError(fmt.Sprintf("Cannot apply %s to %s (not a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newLgFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.FloatLiteral:
				return expression.FloatLiteral{Value: math.Log10(valExpr.Value)}, nil
			}
			return nil, runtimeError(fmt.Sprintf("Cannot apply %s to %s (not a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newLnFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.FloatLiteral:
				return expression.FloatLiteral{Value: math.Log(valExpr.Value)}, nil
			}
			return nil, runtimeError(fmt.Sprintf("Cannot apply %s to %s (not a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newRoundFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.FloatLiteral:
				return expression.FloatLiteral{Value: math.Round(valExpr.Value)}, nil
			}
			return nil, runtimeError(fmt.Sprintf("Cannot apply %s to %s (not a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newSgnFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.FloatLiteral:
				if valExpr.Value < 0.0 {
					return expression.NewSInt(int8(-1)), nil
				} else if valExpr.Value == 0.0 {
					return expression.NewSInt(int8(0)), nil
				} else {
					return expression.NewSInt(int8(1)), nil
				}
			}
			return nil, runtimeError(fmt.Sprintf("Cannot apply %s to %s (not a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newSinFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.FloatLiteral:
				return expression.FloatLiteral{Value: math.Sin(valExpr.Value)}, nil
			}
			return nil, runtimeError(fmt.Sprintf("Cannot apply %s to %s (not a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newSqFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.FloatLiteral:
				return expression.FloatLiteral{Value: valExpr.Value * valExpr.Value}, nil
			}
			return nil, runtimeError(fmt.Sprintf("Cannot apply %s to %s (not a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newSqrtFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.FloatLiteral:
				return expression.FloatLiteral{Value: math.Sqrt(valExpr.Value)}, nil
			}
			return nil, runtimeError(fmt.Sprintf("Cannot apply %s to %s (not a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}

func newTanFunction(name string) *expression.BuiltinFunction {
	return &expression.BuiltinFunction{
		Name: name,
		F: func(expr expression.Expression) (expression.Expression, error) {
			switch valExpr := expr.(type) {
			case expression.FloatLiteral:
				return expression.FloatLiteral{Value: math.Tan(valExpr.Value)}, nil
			}
			return nil, runtimeError(fmt.Sprintf("Cannot apply %s to %s (not a floating point number).", expr.String(), name), expr.OriginalLocationIfPresent())
		},
	}
}
