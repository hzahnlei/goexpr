package eval

import (
	"fmt"
	"math"

	"zahnleiter.org/goexpr/pkg/expression"
)

func RegisterBuiltinConstants(ctx expression.VisitorContext) {
	mustDefineFloat(ctx, "pi", math.Pi)
	mustDefineFloat(ctx, "π", math.Pi)
	mustDefineFloat(ctx, "e", math.E)
	mustDefineUInt[uint8](ctx, "false", 0)
	mustDefineUInt[uint8](ctx, "true", 1)
}

func mustDefineFloat(ctx expression.VisitorContext, name string, val float64) {
	expr := expression.FloatLiteral{Value: val}
	if err := ctx.Define(name, expr); err != nil {
		panic(fmt.Sprintf("Registering built-in floating point constant: %s", err.Error()))
	}
}

func mustDefineUInt[T UIntTypes](ctx expression.VisitorContext, name string, val T) {
	expr := expression.NewUInt(val)
	if err := ctx.Define(name, expr); err != nil {
		panic(fmt.Sprintf("Registering built-in unsigned integer constant: %s", err.Error()))
	}
}
