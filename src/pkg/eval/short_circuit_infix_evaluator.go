package eval

import (
	"fmt"

	"zahnleiter.org/goexpr/pkg/commons"
	"zahnleiter.org/goexpr/pkg/expression"
)

// ---- An evaluator for infix expressions with short circuit -----------------

// The short circuit evaluator evaluates infix expressions that allow for short
// circuiting. For example: For A AND B we do not need to evaluate B if A=0. If
// A=0, then A AND B will allways be 0, regardless of B.
// Actually,  all expressions here are pure, that is,  they do not produce side
// effects.  Also performance gains for not evaluating expressions is not a big
// deal in a simple direct interpreter. So why is there a short circuiting eva-
// luator anyways? Just to demponstrate the posibility.
type shortCircuitInfixEvaluator struct {
	expression.BaseInfixVisitor
	operatorType  expression.InfixOperatorType
	operatorToken expression.OptionalOriginalSource
}

var _ InfixEvaluator = (*shortCircuitInfixEvaluator)(nil)

func newShortCircuitInfixEvaluator(operator expression.InfixOperatorType, originalToken expression.OptionalOriginalSource) shortCircuitInfixEvaluator {
	return shortCircuitInfixEvaluator{
		operatorType:  operator,
		operatorToken: originalToken,
	}
}

// The  infix operators  evaluated herein  can be short circuited.  So we first
// evaluate lhs  and then decide whether  rhs need to be evaluated at all.  The
// final result can be computed  based on lhs alone in some cases (short circu-
// it). In other cases we actually need to evaluate rhs too.
func (eval shortCircuitInfixEvaluator) evaluate(lhs, rhs expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	if eval.operatorType == expression.InfixOpArithDivide {
		// Note how we evaluate RHS (the divisor) first. This is becau-
		// se we can short circuit (no evaluare LHS, the dividend),  if
		// the divisor is zero.
		// Just  for the division we are  swapping LHS and RHS in order
		// to be able  to further use  the visitor pattern.  We need to
		// keep this in mind.
		effectiveRHS, err := Evaluate(rhs, ctx)
		if err != nil {
			return nil, err
		}
		return effectiveRHS.IfxAcceptWithUndetermined(eval, ctx, lhs)
	} else { // boolean AND and OR
		effectiveLHS, err := Evaluate(lhs, ctx)
		if err != nil {
			return nil, err
		}
		return effectiveLHS.IfxAcceptWithUndetermined(eval, ctx, rhs)
	}
}

// ---- SInt ----

func (eval shortCircuitInfixEvaluator) IfxVisitSIntWithUndetermined(lhs expression.SIntLiteral, rhs expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	switch eval.operatorType {
	case expression.InfixOpArithDivide:
		actualRHSDivisor := lhs
		if actualRHSDivisor.IsZero() {
			// Here we are short-circuiting the division operation.
			return nil, runtimeError("Division by zero.", eval.originalLocationIfPresent())
			// TODO: It would be preferrable to use the location of the expression that evaluated to 0.
			// If that is computed, the it has no location.
			// In this case we could use the location of the division operator as a fallback.
		}
		actualLHSDividend := rhs
		return actualLHSDividend.IfxAcceptWithSIntLiteral(eval, ctx, actualRHSDivisor)
	case expression.InfixOpBoolAnd:
		if lhs.IsZero() {
			return UInt8LiteralFromBool(false), nil
		}
		effectiveRHS, err := Evaluate(rhs, ctx)
		if err != nil {
			return nil, err
		}
		return effectiveRHS.IfxAcceptWithSIntLiteral(eval, ctx, lhs)
	case expression.InfixOpBoolOr:
		if !lhs.IsZero() {
			return UInt8LiteralFromBool(true), nil
		}
		effectiveRHS, err := Evaluate(rhs, ctx)
		if err != nil {
			return nil, err
		}
		return effectiveRHS.IfxAcceptWithSIntLiteral(eval, ctx, lhs)
	default:
		return nil, runtimeError(
			fmt.Sprintf("Unsupported infix operator %q.",
				originalOrDefaultInfixOperatorRepresentation(eval.operatorToken, eval.operatorType)),
			eval.originalLocationIfPresent())
	}
}

func originalOrDefaultInfixOperatorRepresentation(originalToken expression.OptionalOriginalSource, opType expression.InfixOperatorType) string {
	if originalToken.HasValue() {
		return originalToken.Value().Lexeme
	}
	return opType.String()
}

func (eval shortCircuitInfixEvaluator) IfxVisitSIntAndSInt(lhs, rhs expression.SIntLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	switch eval.operatorType {
	case expression.InfixOpArithDivide:
		actualRHSDivisor := lhs
		actualLHSDividend := rhs
		// Swap lhs and rhs so they are in the correct order.
		return EvaluateSIntAndSInt(eval.operatorType, actualLHSDividend, actualRHSDivisor, ctx)
	case expression.InfixOpBoolAnd:
		// LHS is known to be true, otherwise this would not be called.
		return UInt8LiteralFromBool(!rhs.IsZero()), nil
	case expression.InfixOpBoolOr:
		// LHS is known to be false, otherwise this would not be called.
		return UInt8LiteralFromBool(!rhs.IsZero()), nil
	default:
		return nil, runtimeError(
			fmt.Sprintf("Unsupported infix operator %q.",
				originalOrDefaultInfixOperatorRepresentation(eval.operatorToken, eval.operatorType)),
			eval.originalLocationIfPresent())
	}
}

func (eval shortCircuitInfixEvaluator) originalLocationIfPresent() expression.OptionalOriginalLocation {
	if eval.operatorToken.HasValue() {
		return commons.OptionalOf(eval.operatorToken.Value().Location)
	}
	return commons.EmptyOptional[expression.OriginalLocation]()
}

// ---- UInt ----

func (eval shortCircuitInfixEvaluator) IfxVisitUIntWithUndetermined(lhs expression.UIntLiteral, rhs expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	switch eval.operatorType {
	case expression.InfixOpArithDivide:
		actualRHSDivisor := lhs
		if actualRHSDivisor.IsZero() {
			// Here we are short-circuiting the division operation.
			return nil, runtimeError("Division by zero.", eval.originalLocationIfPresent())
			// TODO: It would be preferrable to use the location of the expression that evaluated to 0.
			// If that is computed, the it has no location.
			// In this case we could use the location of the division operator as a fallback.
		}
		actualLHSDividend := rhs
		return actualLHSDividend.IfxAcceptWithUIntLiteral(eval, ctx, actualRHSDivisor)
	case expression.InfixOpBoolAnd:
		if lhs.IsZero() {
			return UInt8LiteralFromBool(false), nil
		}
		effectiveRHS, err := Evaluate(rhs, ctx)
		if err != nil {
			return nil, err
		}
		return effectiveRHS.IfxAcceptWithUIntLiteral(eval, ctx, lhs)
	case expression.InfixOpBoolOr:
		if !lhs.IsZero() {
			return UInt8LiteralFromBool(true), nil
		}
		effectiveRHS, err := Evaluate(rhs, ctx)
		if err != nil {
			return nil, err
		}
		return effectiveRHS.IfxAcceptWithUIntLiteral(eval, ctx, lhs)
	default:
		return nil, runtimeError(
			fmt.Sprintf("Unsupported infix operator %q.",
				originalOrDefaultInfixOperatorRepresentation(eval.operatorToken, eval.operatorType)),
			eval.originalLocationIfPresent())
	}
}

func (eval shortCircuitInfixEvaluator) IfxVisitUIntAndUInt(lhs expression.UIntLiteral, rhs expression.UIntLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	switch eval.operatorType {
	case expression.InfixOpArithDivide:
		actualRHSDivisor := lhs
		actualLHSDividend := rhs
		// Swap lhs and rhs so they are in the correct order.
		return EvaluateUIntAndUInt(eval.operatorType, actualLHSDividend, actualRHSDivisor, ctx)
	case expression.InfixOpBoolAnd:
		// LHS is known to be true, otherwise this would not be called.
		return UInt8LiteralFromBool(!rhs.IsZero()), nil
	case expression.InfixOpBoolOr:
		// LHS is known to be false, otherwise this would not be called.
		return UInt8LiteralFromBool(!rhs.IsZero()), nil
	default:
		return nil, runtimeError(
			fmt.Sprintf("Unsupported infix operator %q.",
				originalOrDefaultInfixOperatorRepresentation(eval.operatorToken, eval.operatorType)),
			eval.originalLocationIfPresent())
	}
}

// ---- Float ----

func (eval shortCircuitInfixEvaluator) IfxVisitFloatWithUndetermined(lhs expression.FloatLiteral, rhs expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	switch eval.operatorType {
	case expression.InfixOpArithDivide:
		actualRHSDivisor := lhs
		if actualRHSDivisor.IsZero() {
			// Here we are short-circuiting the division operation.
			return nil, runtimeError("Division by zero.", eval.originalLocationIfPresent())
			// TODO: It would be preferrable to use the location of the expression that evaluated to 0.
			// If that is computed, the it has no location.
			// In this case we could use the location of the division operator as a fallback.
		}
		actualLHSDividend := rhs
		return actualLHSDividend.IfxAcceptWithFloatLiteral(eval, ctx, actualRHSDivisor)
	case expression.InfixOpBoolAnd:
		if lhs.IsZero() {
			return UInt8LiteralFromBool(false), nil
		}
		effectiveRHS, err := Evaluate(rhs, ctx)
		if err != nil {
			return nil, err
		}
		return effectiveRHS.IfxAcceptWithFloatLiteral(eval, ctx, lhs)
	case expression.InfixOpBoolOr:
		if !lhs.IsZero() {
			return UInt8LiteralFromBool(true), nil
		}
		effectiveRHS, err := Evaluate(rhs, ctx)
		if err != nil {
			return nil, err
		}
		return effectiveRHS.IfxAcceptWithFloatLiteral(eval, ctx, lhs)
	default:
		return nil, runtimeError(
			fmt.Sprintf("Unsupported infix operator %q.",
				originalOrDefaultInfixOperatorRepresentation(eval.operatorToken, eval.operatorType)),
			eval.originalLocationIfPresent())
	}
}

func (eval shortCircuitInfixEvaluator) IfxVisitFloatAndFloat(lhs expression.FloatLiteral, rhs expression.FloatLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	switch eval.operatorType {
	case expression.InfixOpArithDivide:
		actualRHSDivisor := lhs
		actualLHSDividend := rhs
		// Swap lhs and rhs so they are in the correct order.
		return EvaluateFloatAndFloat(eval.operatorType, actualLHSDividend, actualRHSDivisor, ctx)
	case expression.InfixOpBoolAnd:
		// LHS is known to be true, otherwise this would not be called.
		return UInt8LiteralFromBool(!rhs.IsZero()), nil
	case expression.InfixOpBoolOr:
		// LHS is known to be false, otherwise this would not be called.
		return UInt8LiteralFromBool(!rhs.IsZero()), nil
	default:
		return nil, runtimeError(
			fmt.Sprintf("Unsupported infix operator %q.",
				originalOrDefaultInfixOperatorRepresentation(eval.operatorToken, eval.operatorType)),
			eval.originalLocationIfPresent())
	}
}
