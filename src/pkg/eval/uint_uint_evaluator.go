package eval

import (
	"math"

	"zahnleiter.org/goexpr/pkg/expression"
)

func EvaluateUIntAndUInt(operatorType expression.InfixOperatorType, lhs, rhs expression.UIntLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	switch lhsVal := lhs.Value.(type) {
	case uint8:
		switch rhsVal := rhs.Value.(type) {
		case uint8:
			return evaluateUInt8AndUInt8(operatorType, lhsVal, rhsVal)
		case uint16:
			return evaluateUInt8AndUInt16(operatorType, lhsVal, rhsVal)
		case uint32:
			return evaluateUInt8AndUInt32(operatorType, lhsVal, rhsVal)
		case uint64:
			return evaluateUInt8AndUInt64(operatorType, lhsVal, rhsVal)
		}
	case uint16:
		switch rhsVal := rhs.Value.(type) {
		case uint8:
			return evaluateUInt16AndUInt8(operatorType, lhsVal, rhsVal)
		case uint16:
			return evaluateUInt16AndUInt16(operatorType, lhsVal, rhsVal)
		case uint32:
			return evaluateUInt16AndUInt32(operatorType, lhsVal, rhsVal)
		case uint64:
			return evaluateUInt16AndUInt64(operatorType, lhsVal, rhsVal)
		}
	case uint32:
		switch rhsVal := rhs.Value.(type) {
		case uint8:
			return evaluateUInt32AndUInt8(operatorType, lhsVal, rhsVal)
		case uint16:
			return evaluateUInt32AndUInt16(operatorType, lhsVal, rhsVal)
		case uint32:
			return evaluateUInt32AndUInt32(operatorType, lhsVal, rhsVal)
		case uint64:
			return evaluateUInt32AndUInt64(operatorType, lhsVal, rhsVal)
		}
	case uint64:
		switch rhsVal := rhs.Value.(type) {
		case uint8:
			return evaluateUInt64AndUInt8(operatorType, lhsVal, rhsVal)
		case uint16:
			return evaluateUInt64AndUInt16(operatorType, lhsVal, rhsVal)
		case uint32:
			return evaluateUInt64AndUInt32(operatorType, lhsVal, rhsVal)
		case uint64:
			return evaluateUInt64AndUInt64(operatorType, lhsVal, rhsVal)
		}
	}
	panic(InfixOpTypeMismatch(operatorType, lhs, rhs)) // Should never happen
}

func evaluateUInt8AndUInt8(operator expression.InfixOperatorType, lhs uint8, rhs uint8) (expression.Expression, error) {
	return evaluateUIntUInt(operator, lhs, rhs)
}

func evaluateUInt8AndUInt16(operator expression.InfixOperatorType, lhs uint8, rhs uint16) (expression.Expression, error) {
	return evaluateUIntUInt(operator, uint16(lhs), rhs)
}

func evaluateUInt8AndUInt32(operator expression.InfixOperatorType, lhs uint8, rhs uint32) (expression.Expression, error) {
	return evaluateUIntUInt(operator, uint32(lhs), rhs)
}

func evaluateUInt8AndUInt64(operator expression.InfixOperatorType, lhs uint8, rhs uint64) (expression.Expression, error) {
	return evaluateUIntUInt(operator, uint64(lhs), rhs)
}

func evaluateUInt16AndUInt8(operator expression.InfixOperatorType, lhs uint16, rhs uint8) (expression.Expression, error) {
	return evaluateUIntUInt(operator, lhs, uint16(rhs))
}

func evaluateUInt16AndUInt16(operator expression.InfixOperatorType, lhs uint16, rhs uint16) (expression.Expression, error) {
	return evaluateUIntUInt(operator, lhs, rhs)
}

func evaluateUInt16AndUInt32(operator expression.InfixOperatorType, lhs uint16, rhs uint32) (expression.Expression, error) {
	return evaluateUIntUInt(operator, uint32(lhs), rhs)
}

func evaluateUInt16AndUInt64(operator expression.InfixOperatorType, lhs uint16, rhs uint64) (expression.Expression, error) {
	return evaluateUIntUInt(operator, uint64(lhs), rhs)
}

func evaluateUInt32AndUInt8(operator expression.InfixOperatorType, lhs uint32, rhs uint8) (expression.Expression, error) {
	return evaluateUIntUInt(operator, lhs, uint32(rhs))
}

func evaluateUInt32AndUInt16(operator expression.InfixOperatorType, lhs uint32, rhs uint16) (expression.Expression, error) {
	return evaluateUIntUInt(operator, lhs, uint32(rhs))
}

func evaluateUInt32AndUInt32(operator expression.InfixOperatorType, lhs uint32, rhs uint32) (expression.Expression, error) {
	return evaluateUIntUInt(operator, lhs, rhs)
}

func evaluateUInt32AndUInt64(operator expression.InfixOperatorType, lhs uint32, rhs uint64) (expression.Expression, error) {
	return evaluateUIntUInt(operator, uint64(lhs), rhs)
}

func evaluateUInt64AndUInt8(operator expression.InfixOperatorType, lhs uint64, rhs uint8) (expression.Expression, error) {
	return evaluateUIntUInt(operator, lhs, uint64(rhs))
}

func evaluateUInt64AndUInt16(operator expression.InfixOperatorType, lhs uint64, rhs uint16) (expression.Expression, error) {
	return evaluateUIntUInt(operator, lhs, uint64(rhs))
}

func evaluateUInt64AndUInt32(operator expression.InfixOperatorType, lhs uint64, rhs uint32) (expression.Expression, error) {
	return evaluateUIntUInt(operator, lhs, uint64(rhs))
}

func evaluateUInt64AndUInt64(operator expression.InfixOperatorType, lhs uint64, rhs uint64) (expression.Expression, error) {
	return evaluateUIntUInt(operator, lhs, rhs)
}

type UIntTypes interface {
	uint8 | uint16 | uint32 | uint64
}

func evaluateUIntUInt[T UIntTypes](operatorType expression.InfixOperatorType, lhs T, rhs T) (expression.Expression, error) {
	switch operatorType {
	case expression.InfixOpArithDivide:
		// RHS will never be zero because of short circuiting.
		return expression.NewUInt(lhs / rhs), nil
	case expression.InfixOpArithModulus:
		return expression.NewUInt(lhs % rhs), nil
	case expression.InfixOpArithMinus:
		return expression.NewUInt(lhs - rhs), nil
	case expression.InfixOpArithMultiply:
		return expression.NewUInt(lhs * rhs), nil
	case expression.InfixOpArithPlus:
		return expression.NewUInt(lhs + rhs), nil
	case expression.InfixOpArithPower:
		return expression.NewUInt(T(math.Pow(float64(lhs), float64(rhs)))), nil
	case expression.InfixOpArithShiftLeft:
		return expression.NewUInt(lhs << rhs), nil
	case expression.InfixOpArithShiftRight:
		return expression.NewUInt(lhs >> rhs), nil
	// ---- Boolean operators ----
	case expression.InfixOpBoolXOr:
		return UInt8LiteralFromBool((lhs != 0 && rhs == 0) || (lhs == 0 && rhs != 0)), nil
	// ---- Bitwise operators ----
	case expression.InfixOpBitAnd:
		return expression.NewUInt(lhs & rhs), nil
	case expression.InfixOpBitOr:
		return expression.NewUInt(lhs | rhs), nil
	case expression.InfixOpBitXOr:
		return expression.NewUInt(lhs ^ rhs), nil
	// ---- Relational operators ----
	case expression.InfixOpRelEQ:
		return UInt8LiteralFromBool(lhs == rhs), nil
	case expression.InfixOpRelGE:
		return UInt8LiteralFromBool(lhs >= rhs), nil
	case expression.InfixOpRelGT:
		return UInt8LiteralFromBool(lhs > rhs), nil
	case expression.InfixOpRelLE:
		return UInt8LiteralFromBool(lhs <= rhs), nil
	case expression.InfixOpRelLT:
		return UInt8LiteralFromBool(lhs < rhs), nil
	case expression.InfixOpRelNE:
		return UInt8LiteralFromBool(lhs != rhs), nil
	}
	panic(InfixOpTypeMismatch(operatorType, lhs, rhs)) // Should never happen
}
