package eval

import (
	"testing"

	"github.com/stretchr/testify/require"
	"zahnleiter.org/goexpr/pkg/expression"
)

// InfixOpNotApplicable is invoked with expressions.
func TestInfixOpTypeMismatchWithExpressions(t *testing.T) {
	// GIVEN
	infixOp := expression.InfixOpArithDivide
	lhs := expression.NewUInt(uint16(1234))
	rhs := expression.NewUInt(uint16(5678))
	// WHEN
	actualMessage := InfixOpTypeMismatch(infixOp, lhs, rhs)
	// THEN
	require.Equal(t, `Type mismatch. Infix operator "/" does not support expressions 1234u16 (type uint16) and 5678u16 (type uint16).`, actualMessage)
}

// InfixOpNotApplicable is also invoked with primitive types.
func TestInfixOpTypeMismatchWithPrimitives(t *testing.T) {
	// GIVEN
	infixOp := expression.InfixOpArithDivide
	lhs := uint16(666)
	rhs := uint16(777)
	// WHEN
	actualMessage := InfixOpTypeMismatch(infixOp, lhs, rhs)
	// THEN
	require.Equal(t, `Type mismatch. Infix operator "/" does not support expressions 666u16 (type uint16) and 777u16 (type uint16).`, actualMessage)
}

// PrefixOpNotApplicable is invoked with expressions.
func TestPrefixOpTypeMismatchWithExpression(t *testing.T) {
	// GIVEN
	prefixOp := expression.PrefixOpArithNegate
	expr := expression.NewUInt(uint16(1234))
	// WHEN
	actualMessage := PrefixOpTypeMismatch(prefixOp, expr)
	// THEN
	require.Equal(t, `Type mismatch. Prefix operator "-" does not support expression 1234u16 (type uint16).`, actualMessage)
}

// PrefixOpNotApplicable is  never invoked with primitive types.  However, this
// would work nevertheless.
func TestPrefixOpTypeMismatchWithPrimitives(t *testing.T) {
	// GIVEN
	prefixOp := expression.PrefixOpArithNegate
	expr := expression.NewUInt(uint32(4711))
	// WHEN
	actualMessage := PrefixOpTypeMismatch(prefixOp, expr)
	// THEN
	require.Equal(t, `Type mismatch. Prefix operator "-" does not support expression 4711u32 (type uint32).`, actualMessage)
}
