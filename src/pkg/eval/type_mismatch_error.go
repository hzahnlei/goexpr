package eval

import (
	"fmt"

	"zahnleiter.org/goexpr/pkg/expression"
)

func InfixOpTypeMismatch(operatorType expression.InfixOperatorType, lhs, rhs any) string {
	return fmt.Sprintf("Type mismatch. Infix operator %q does not support expressions %s (type %s) and %s (type %s).",
		operatorType,
		RepresentationOfValueForErrorMessage(lhs), RepresentationOfTypeForErrorMessage(lhs),
		RepresentationOfValueForErrorMessage(rhs), RepresentationOfTypeForErrorMessage(rhs))
}

func PrefixOpTypeMismatch(operatorType expression.PrefixOperatorType, expr any) string {
	return fmt.Sprintf("Type mismatch. Prefix operator %q does not support expression %v (type %s).",
		operatorType,
		RepresentationOfValueForErrorMessage(expr), RepresentationOfTypeForErrorMessage(expr))
}

func RepresentationOfValueForErrorMessage(expr any) string {
	switch value := expr.(type) {
	case expression.Expression:
		return value.String()
	case int8:
		return fmt.Sprintf("%ds8", value)
	case int16:
		return fmt.Sprintf("%ds16", value)
	case int32:
		return fmt.Sprintf("%ds32", value)
	case int64:
		return fmt.Sprintf("%ds64", value)
	case uint8:
		return fmt.Sprintf("%du8", value)
	case uint16:
		return fmt.Sprintf("%du16", value)
	case uint32:
		return fmt.Sprintf("%du32", value)
	case uint64:
		return fmt.Sprintf("%du64", value)
	case float64:
		return fmt.Sprintf("%ff64", value)
	default:
		return fmt.Sprintf("%v", value)
	}
}

func RepresentationOfTypeForErrorMessage(expr any) string {
	switch value := expr.(type) {
	case expression.Expression:
		return repOfType(value)
	case int8:
		return "sint8"
	case int16:
		return "sint16"
	case int32:
		return "sint32"
	case int64:
		return "sint64"
	case uint8:
		return "uint8"
	case uint16:
		return "uint16"
	case uint32:
		return "uint32"
	case uint64:
		return "uint64"
	case float64:
		return "float64"
	default:
		return "UNKNOWN"
	}
}

func repOfType(expr expression.Expression) string {
	switch knownExpression := expr.(type) {
	case expression.BuiltinFunction:
		return fmt.Sprintf("built-in function %q", knownExpression.Name)
	case expression.EOF:
		return "EOF"
	case expression.FloatLiteral:
		return RepresentationOfTypeForErrorMessage(knownExpression.Value)
	case expression.FunctionApplication:
		return "function application"
	case expression.Identifier:
		return fmt.Sprintf("identifier %q", knownExpression.Name)
	case expression.Infix:
		return fmt.Sprintf("application of infix operator %q", knownExpression.OperatorType.String())
	case expression.List:
		return "list of expressions"
	case expression.Parenthesis:
		return "expression in parenthesis"
	case expression.Prefix:
		return fmt.Sprintf("application of prefix operator %q", knownExpression.OperatorType.String())
	case expression.SIntLiteral:
		RepresentationOfTypeForErrorMessage(knownExpression.Value)
	case expression.Sequence:
		return "sequence of expressions"
	case expression.UIntLiteral:
		return RepresentationOfTypeForErrorMessage(knownExpression.Value)
	}
	return "UNKNOWN"
}
