package eval

import (
	"fmt"

	"zahnleiter.org/goexpr/pkg/commons"
	"zahnleiter.org/goexpr/pkg/expression"
)

// This evaluator evaluates prefix expressions.
type prefixEvaluator struct {
	expression.BaseVisitor
	operatorType  expression.PrefixOperatorType
	operatorToken expression.OptionalOriginalSource
}

var _ Evaluator = (*prefixEvaluator)(nil)

func newPrefixEvaluator(operator expression.PrefixOperatorType, originalToken expression.OptionalOriginalSource) prefixEvaluator {
	return prefixEvaluator{
		operatorType:  operator,
		operatorToken: originalToken,
	}
}

func (eval prefixEvaluator) evaluate(expr expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	effectiveExpr, err := Evaluate(expr, ctx)
	if err != nil {
		return nil, err
	}
	return effectiveExpr.Accept(eval, ctx)
}

func (eval prefixEvaluator) VisitSIntLiteral(expr expression.SIntLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	switch eval.operatorType {
	case expression.PrefixOpArithNegate:
		switch value := expr.Value.(type) {
		case int8:
			return expression.NewSInt(int8(-value)), nil // FIXME remove type casting here? increase coverage first
		case int16:
			return expression.NewSInt(int16(-value)), nil
		case int32:
			return expression.NewSInt(int32(-value)), nil
		case int64:
			return expression.NewSInt(int64(-value)), nil
		}
	case expression.PrefixOpBitComplement:
		switch value := expr.Value.(type) {
		case int8:
			return expression.NewSInt(int8(^value)), nil
		case int16:
			return expression.NewSInt(int16(^value)), nil
		case int32:
			return expression.NewSInt(int32(^value)), nil
		case int64:
			return expression.NewSInt(int64(^value)), nil
		}
	case expression.PrefixOpBoolNot:
		return UInt8LiteralFromBool(expr.IsZero()), nil
	}
	panic(PrefixOpTypeMismatch(eval.operatorType, expr)) // Should never happen
}

func originalOrDefaultPrefixOperatorRepresentation(originalToken expression.OptionalOriginalSource, opType expression.PrefixOperatorType) string {
	if originalToken.HasValue() {
		return originalToken.Value().Lexeme
	}
	return opType.String()
}

func (eval prefixEvaluator) originalLocationIfPresent() expression.OptionalOriginalLocation {
	if eval.operatorToken.HasValue() {
		return commons.OptionalOf(eval.operatorToken.Value().Location)
	}
	return commons.EmptyOptional[expression.OriginalLocation]()
}

func (eval prefixEvaluator) VisitUIntLiteral(expr expression.UIntLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	switch eval.operatorType {
	case expression.PrefixOpArithNegate:
		switch value := expr.Value.(type) {
		case uint8:
			return expression.NewUInt(uint8(-value)), nil // FXIME no typecasting here? increase coverage first
		case uint16:
			return expression.NewUInt(uint16(-value)), nil
		case uint32:
			return expression.NewUInt(uint32(-value)), nil
		case uint64:
			return expression.NewUInt(uint64(-value)), nil
		}
	case expression.PrefixOpBitComplement:
		switch value := expr.Value.(type) {
		case uint8:
			return expression.NewUInt(uint8(^value)), nil
		case uint16:
			return expression.NewUInt(uint16(^value)), nil
		case uint32:
			return expression.NewUInt(uint32(^value)), nil
		case uint64:
			return expression.NewUInt(uint64(^value)), nil
		}
	case expression.PrefixOpBoolNot:
		return UInt8LiteralFromBool(expr.IsZero()), nil
	}
	panic(PrefixOpTypeMismatch(eval.operatorType, expr)) // Should never happen
}

func (eval prefixEvaluator) VisitFloatLiteral(expr expression.FloatLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	value := expr.Value
	switch eval.operatorType {
	case expression.PrefixOpArithNegate:
		return expression.FloatLiteral{Value: -value}, nil
	case expression.PrefixOpBitComplement:
		return nil, runtimeError(
			fmt.Sprintf("Bitweise complement %q not applicable to floating point literal %s.",
				originalOrDefaultPrefixOperatorRepresentation(eval.operatorToken, eval.operatorType),
				*expr.OriginalRepresentationIfPresent().OrElse(fmt.Sprintf("%f", expr.Value))),
			eval.originalLocationIfPresent())
	case expression.PrefixOpBoolNot:
		return UInt8LiteralFromBool(expr.IsZero()), nil
	}
	panic(PrefixOpTypeMismatch(eval.operatorType, expr)) // Should never happen
}
