package eval

import (
	"zahnleiter.org/goexpr/pkg/expression"
)

// This evaluator evaluates infix expressions  that can not be short circuited.
// For example:  For A XOR B we actually need to evaluate both, A and B, before
// we can compute the result of the XOR operation.
type nonShortCircuitInfixEvaluator struct {
	expression.BaseInfixVisitor
	operatorType  expression.InfixOperatorType
	operatorToken expression.OptionalOriginalSource
}

var _ InfixEvaluator = (*nonShortCircuitInfixEvaluator)(nil)

func newNonShortCircuitInfixEvaluator(operator expression.InfixOperatorType, originalToken expression.OptionalOriginalSource) nonShortCircuitInfixEvaluator {
	return nonShortCircuitInfixEvaluator{
		operatorType:  operator,
		operatorToken: originalToken,
	}
}

// The infix operators evaluated herein can not be short circuited. So we first
// evaluate both - lhs and rhs - and then  apply the operator on both evaluated
// arguments.
func (eval nonShortCircuitInfixEvaluator) evaluate(lhs, rhs expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	effectiveLHS, err := Evaluate(lhs, ctx)
	if err != nil {
		return nil, err
	}
	effectiveRHS, err := Evaluate(rhs, ctx)
	if err != nil {
		return nil, err
	}
	return effectiveLHS.IfxAcceptWithUndetermined(eval, ctx, effectiveRHS)
}

// ---- SInt ----

func (eval nonShortCircuitInfixEvaluator) IfxVisitSIntWithUndetermined(lhs expression.SIntLiteral, rhs expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	return rhs.IfxAcceptWithSIntLiteral(eval, ctx, lhs)
}

func (eval nonShortCircuitInfixEvaluator) IfxVisitSIntAndSInt(lhs, rhs expression.SIntLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	return EvaluateSIntAndSInt(eval.operatorType, lhs, rhs, ctx)
}

// ---- UInt ----

func (eval nonShortCircuitInfixEvaluator) IfxVisitUIntWithUndetermined(lhs expression.UIntLiteral, rhs expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	return rhs.IfxAcceptWithUIntLiteral(eval, ctx, lhs)
}

func (eval nonShortCircuitInfixEvaluator) IfxVisitUIntAndUInt(lhs, rhs expression.UIntLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	return EvaluateUIntAndUInt(eval.operatorType, lhs, rhs, ctx)
}

// ---- Float ----

func (eval nonShortCircuitInfixEvaluator) IfxVisitFloatWithUndetermined(lhs expression.FloatLiteral, rhs expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	return rhs.IfxAcceptWithFloatLiteral(eval, ctx, lhs)
}

func (eval nonShortCircuitInfixEvaluator) IfxVisitFloatAndFloat(lhs, rhs expression.FloatLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	return EvaluateFloatAndFloat(eval.operatorType, lhs, rhs, ctx)
}
