package eval

import (
	"zahnleiter.org/goexpr/pkg/expression"
)

type Evaluator interface {
	evaluate(expression.Expression, expression.VisitorContext) (expression.Expression, error)
}

type InfixEvaluator interface {
	evaluate(lhs, rhs expression.Expression, ctx expression.VisitorContext) (expression.Expression, error)
}
