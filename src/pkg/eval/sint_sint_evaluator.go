package eval

import (
	"math"

	"zahnleiter.org/goexpr/pkg/expression"
)

func EvaluateSIntAndSInt(operatorType expression.InfixOperatorType, lhs, rhs expression.SIntLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	switch lhsVal := lhs.Value.(type) {
	case int8:
		switch rhsVal := rhs.Value.(type) {
		case int8:
			return evaluateSInt8AndSInt8(operatorType, lhsVal, rhsVal)
		case int16:
			return evaluateSInt8AndSInt16(operatorType, lhsVal, rhsVal)
		case int32:
			return evaluateSInt8AndSInt32(operatorType, lhsVal, rhsVal)
		case int64:
			return evaluateSInt8AndSInt64(operatorType, lhsVal, rhsVal)
		}
	case int16:
		switch rhsVal := rhs.Value.(type) {
		case int8:
			return evaluateSInt16AndSInt8(operatorType, lhsVal, rhsVal)
		case int16:
			return evaluateSInt16AndSInt16(operatorType, lhsVal, rhsVal)
		case int32:
			return evaluateSInt16AndSInt32(operatorType, lhsVal, rhsVal)
		case int64:
			return evaluateSInt16AndSInt64(operatorType, lhsVal, rhsVal)
		}
	case int32:
		switch rhsVal := rhs.Value.(type) {
		case int8:
			return evaluateSInt32AndSInt8(operatorType, lhsVal, rhsVal)
		case int16:
			return evaluateSInt32AndSInt16(operatorType, lhsVal, rhsVal)
		case int32:
			return evaluateSInt32AndSInt32(operatorType, lhsVal, rhsVal)
		case int64:
			return evaluateSInt32AndSInt64(operatorType, lhsVal, rhsVal)
		}
	case int64:
		switch rhsVal := rhs.Value.(type) {
		case int8:
			return evaluateSInt64AndSInt8(operatorType, lhsVal, rhsVal)
		case int16:
			return evaluateSInt64AndSInt16(operatorType, lhsVal, rhsVal)
		case int32:
			return evaluateSInt64AndSInt32(operatorType, lhsVal, rhsVal)
		case int64:
			return evaluateSInt64AndSInt64(operatorType, lhsVal, rhsVal)
		}
	}
	panic(InfixOpTypeMismatch(operatorType, lhs, rhs)) // Should never happen
}

func evaluateSInt8AndSInt8(operator expression.InfixOperatorType, lhs int8, rhs int8) (expression.Expression, error) {
	return evaluateSIntSInt(operator, lhs, rhs)
}

func evaluateSInt8AndSInt16(operator expression.InfixOperatorType, lhs int8, rhs int16) (expression.Expression, error) {
	return evaluateSIntSInt(operator, int16(lhs), rhs)
}

func evaluateSInt8AndSInt32(operator expression.InfixOperatorType, lhs int8, rhs int32) (expression.Expression, error) {
	return evaluateSIntSInt(operator, int32(lhs), rhs)
}

func evaluateSInt8AndSInt64(operator expression.InfixOperatorType, lhs int8, rhs int64) (expression.Expression, error) {
	return evaluateSIntSInt(operator, int64(lhs), rhs)
}

func evaluateSInt16AndSInt8(operator expression.InfixOperatorType, lhs int16, rhs int8) (expression.Expression, error) {
	return evaluateSIntSInt(operator, lhs, int16(rhs))
}

func evaluateSInt16AndSInt16(operator expression.InfixOperatorType, lhs int16, rhs int16) (expression.Expression, error) {
	return evaluateSIntSInt(operator, lhs, rhs)
}

func evaluateSInt16AndSInt32(operator expression.InfixOperatorType, lhs int16, rhs int32) (expression.Expression, error) {
	return evaluateSIntSInt(operator, int32(lhs), rhs)
}

func evaluateSInt16AndSInt64(operator expression.InfixOperatorType, lhs int16, rhs int64) (expression.Expression, error) {
	return evaluateSIntSInt(operator, int64(lhs), rhs)
}

func evaluateSInt32AndSInt8(operator expression.InfixOperatorType, lhs int32, rhs int8) (expression.Expression, error) {
	return evaluateSIntSInt(operator, lhs, int32(rhs))
}

func evaluateSInt32AndSInt16(operator expression.InfixOperatorType, lhs int32, rhs int16) (expression.Expression, error) {
	return evaluateSIntSInt(operator, int16(lhs), rhs)
}

func evaluateSInt32AndSInt32(operator expression.InfixOperatorType, lhs int32, rhs int32) (expression.Expression, error) {
	return evaluateSIntSInt(operator, lhs, rhs)
}

func evaluateSInt32AndSInt64(operator expression.InfixOperatorType, lhs int32, rhs int64) (expression.Expression, error) {
	return evaluateSIntSInt(operator, int64(lhs), rhs)
}

func evaluateSInt64AndSInt8(operator expression.InfixOperatorType, lhs int64, rhs int8) (expression.Expression, error) {
	return evaluateSIntSInt(operator, lhs, int64(rhs))
}

func evaluateSInt64AndSInt16(operator expression.InfixOperatorType, lhs int64, rhs int16) (expression.Expression, error) {
	return evaluateSIntSInt(operator, lhs, int64(rhs))
}

func evaluateSInt64AndSInt32(operator expression.InfixOperatorType, lhs int64, rhs int32) (expression.Expression, error) {
	return evaluateSIntSInt(operator, lhs, int64(rhs))
}

func evaluateSInt64AndSInt64(operator expression.InfixOperatorType, lhs int64, rhs int64) (expression.Expression, error) {
	return evaluateSIntSInt(operator, lhs, rhs)
}

type SIntTypes interface{ int8 | int16 | int32 | int64 }

func evaluateSIntSInt[T SIntTypes](operatorType expression.InfixOperatorType, lhs T, rhs T) (expression.Expression, error) {
	switch operatorType {
	case expression.InfixOpArithDivide:
		// RHS will never be zero because of short circuiting.
		return expression.NewSInt(lhs / rhs), nil
	case expression.InfixOpArithModulus:
		return expression.NewSInt(lhs % rhs), nil
	case expression.InfixOpArithMinus:
		return expression.NewSInt(lhs - rhs), nil
	case expression.InfixOpArithMultiply:
		return expression.NewSInt(lhs * rhs), nil
	case expression.InfixOpArithPlus:
		return expression.NewSInt(lhs + rhs), nil
	case expression.InfixOpArithPower:
		return expression.NewSInt(T(math.Pow(float64(lhs), float64(rhs)))), nil
	case expression.InfixOpArithShiftLeft:
		return expression.NewSInt(lhs << rhs), nil
	case expression.InfixOpArithShiftRight:
		return expression.NewSInt(lhs >> rhs), nil
	// ---- Boolean operators ----
	case expression.InfixOpBoolXOr:
		return UInt8LiteralFromBool((lhs != 0 && rhs == 0) || (lhs == 0 && rhs != 0)), nil
	// ---- Bitwise operators ----
	case expression.InfixOpBitAnd:
		return expression.NewSInt(lhs & rhs), nil
	case expression.InfixOpBitOr:
		return expression.NewSInt(lhs | rhs), nil
	case expression.InfixOpBitXOr:
		return expression.NewSInt(lhs ^ rhs), nil
	// ---- Relational operators ----
	case expression.InfixOpRelEQ:
		return UInt8LiteralFromBool(lhs == rhs), nil
	case expression.InfixOpRelGE:
		return UInt8LiteralFromBool(lhs >= rhs), nil
	case expression.InfixOpRelGT:
		return UInt8LiteralFromBool(lhs > rhs), nil
	case expression.InfixOpRelLE:
		return UInt8LiteralFromBool(lhs <= rhs), nil
	case expression.InfixOpRelLT:
		return UInt8LiteralFromBool(lhs < rhs), nil
	case expression.InfixOpRelNE:
		return UInt8LiteralFromBool(lhs != rhs), nil
	}
	panic(InfixOpTypeMismatch(operatorType, lhs, rhs)) // Should never happen
}
