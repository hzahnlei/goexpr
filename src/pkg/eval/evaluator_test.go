package eval

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/goexpr/pkg/expression"
)

func TestIntLiteralsEvaluateToThemselfes(t *testing.T) {
	// GIVEN
	expr := expression.NewSInt(int8(10))
	eval := topLevelEvaluator{}
	// WHEN
	result, err := eval.evaluate(expr, nil)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, expr, result)
}

func TestInfixOperatorEvaluation(t *testing.T) {
	// GIVEN
	lhs := expression.NewSInt(int8(10))
	rhs := expression.NewSInt(int8(20))
	expr := expression.Infix{OperatorType: expression.InfixOpArithPlus, LHSArgument: lhs, RHSArgument: rhs}
	eval := topLevelEvaluator{}
	// WHEN
	result, err := eval.evaluate(expr, nil)
	// THEN
	require.NoError(t, err)
	assert.Equal(t, expression.NewSInt(int8(30)), result)
}
