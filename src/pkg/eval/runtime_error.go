package eval

import (
	"zahnleiter.org/goexpr/pkg/commons"
	"zahnleiter.org/goexpr/pkg/expression"
)

func runtimeError(msg string, location expression.OptionalOriginalLocation) error {
	if location.HasValue() {
		checkedLocation := *location.Value()
		return commons.MustNewRuntimeError(msg, checkedLocation.FileName, checkedLocation.Line, checkedLocation.Column)
	}
	return commons.MustNewUnlocatableRuntimeError(msg)
}
