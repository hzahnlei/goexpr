package eval

import (
	"fmt"

	"zahnleiter.org/goexpr/pkg/expression"
)

type assigmentEvaluator struct {
	topLevelEvaluator
	expression.BaseInfixVisitor
	originalSource expression.Infix
}

var _ InfixEvaluator = (*assigmentEvaluator)(nil)

func newAssigmentEvaluator(originalSource expression.Infix) assigmentEvaluator {
	return assigmentEvaluator{
		originalSource: originalSource,
	}
}

func (eval assigmentEvaluator) evaluate(lhs, rhs expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	valueToBeAssigned, err := eval.topLevelEvaluator.evaluate(rhs, ctx)
	if err != nil {
		return nil, err
	}
	return valueToBeAssigned.IfxAcceptWithUndetermined(eval, ctx, lhs)
}

func (eval assigmentEvaluator) IfxVisitSIntWithUndetermined(lhs expression.SIntLiteral, rhs expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	return rhs.IfxAcceptWithSIntLiteral(eval, ctx, lhs)
}

func (eval assigmentEvaluator) IfxVisitSIntAndIdentifier(value expression.SIntLiteral, identifier expression.Identifier, ctx expression.VisitorContext) (expression.Expression, error) {
	if ctx.IsDefinedLocally(identifier.Name) {
		return nil, runtimeError(fmt.Sprintf(
			"Trying to override local variable %q with signed integer value %d.", identifier.Name, value.Value),
			eval.originalSource.OriginalLocationIfPresent())
	}
	ctx.Define(identifier.Name, value)
	return value, nil
}

func (eval assigmentEvaluator) IfxVisitUIntWithUndetermined(lhs expression.UIntLiteral, rhs expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	return rhs.IfxAcceptWithUIntLiteral(eval, ctx, lhs)
}

func (eval assigmentEvaluator) IfxVisitUIntAndIdentifier(value expression.UIntLiteral, identifier expression.Identifier, ctx expression.VisitorContext) (expression.Expression, error) {
	if ctx.IsDefinedLocally(identifier.Name) {
		return nil, runtimeError(fmt.Sprintf(
			"Trying to override local variable %q with unsigned integer value %d.", identifier.Name, value.Value),
			eval.originalSource.OriginalLocationIfPresent())
	}
	ctx.Define(identifier.Name, value)
	return value, nil
}

func (eval assigmentEvaluator) IfxVisitFloatWithUndetermined(lhs expression.FloatLiteral, rhs expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	return rhs.IfxAcceptWithFloatLiteral(eval, ctx, lhs)
}

func (eval assigmentEvaluator) IfxVisitFloatAndIdentifier(value expression.FloatLiteral, identifier expression.Identifier, ctx expression.VisitorContext) (expression.Expression, error) {
	if ctx.IsDefinedLocally(identifier.Name) {
		return nil, runtimeError(fmt.Sprintf(
			"Trying to override local variable %q with float value %f.", identifier.Name, value.Value),
			eval.originalSource.OriginalLocationIfPresent())
	}
	ctx.Define(identifier.Name, value)
	return value, nil
}
