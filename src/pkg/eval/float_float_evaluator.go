package eval

import (
	"math"

	"zahnleiter.org/goexpr/pkg/expression"
)

func EvaluateFloatAndFloat(operatorType expression.InfixOperatorType,
	lhsExpr, rhsExpr expression.FloatLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	lhs := lhsExpr.Value
	rhs := rhsExpr.Value
	switch operatorType {
	case expression.InfixOpArithDivide:
		// RHS will never be zero because of short circuiting.
		return expression.FloatLiteral{Value: lhs / rhs}, nil
	case expression.InfixOpArithModulus:
		return nil, runtimeError("Modulus operator '%' not applicable to floating point numbers.", lhsExpr.OriginalLocationIfPresent())
	case expression.InfixOpArithMinus:
		return expression.FloatLiteral{Value: lhs - rhs}, nil
	case expression.InfixOpArithMultiply:
		return expression.FloatLiteral{Value: lhs * rhs}, nil
	case expression.InfixOpArithPlus:
		return expression.FloatLiteral{Value: lhs + rhs}, nil
	case expression.InfixOpArithPower:
		return expression.FloatLiteral{Value: math.Pow(lhs, rhs)}, nil
	case expression.InfixOpArithShiftLeft:
		return nil, runtimeError("Shift left operator '<<' not applicable to floating point numbers.", lhsExpr.OriginalLocationIfPresent())
	case expression.InfixOpArithShiftRight:
		return nil, runtimeError("Shift right operator '>>' not applicable to floating point numbers.", lhsExpr.OriginalLocationIfPresent())
	// ---- Boolean operators ----
	case expression.InfixOpBoolXOr:
		return UInt8LiteralFromBool((lhs != 0 && rhs == 0) || (lhs == 0 && rhs != 0)), nil
	// ---- Bitwise operators ----
	case expression.InfixOpBitAnd:
		return nil, runtimeError("Bit wise AND operator '&' not applicable to floating point numbers.", lhsExpr.OriginalLocationIfPresent())
	case expression.InfixOpBitOr:
		return nil, runtimeError("Bit wise OR operator '|' not applicable to floating point numbers.", lhsExpr.OriginalLocationIfPresent())
	case expression.InfixOpBitXOr:
		return nil, runtimeError("Bit wise XOR operator '^' not applicable to floating point numbers.", lhsExpr.OriginalLocationIfPresent())
	// ---- Relational operators ----
	case expression.InfixOpRelEQ:
		return UInt8LiteralFromBool(lhs == rhs), nil
	case expression.InfixOpRelGE:
		return UInt8LiteralFromBool(lhs >= rhs), nil
	case expression.InfixOpRelGT:
		return UInt8LiteralFromBool(lhs > rhs), nil
	case expression.InfixOpRelLE:
		return UInt8LiteralFromBool(lhs <= rhs), nil
	case expression.InfixOpRelLT:
		return UInt8LiteralFromBool(lhs < rhs), nil
	case expression.InfixOpRelNE:
		return UInt8LiteralFromBool(lhs != rhs), nil
	}
	panic(InfixOpTypeMismatch(operatorType, lhsExpr, rhsExpr)) // Should never happen
}
