package eval

import (
	"fmt"

	"zahnleiter.org/goexpr/pkg/expression"
)

type MapBasedVisitorContext struct {
	parent    expression.VisitorContext
	keyValMap map[string]expression.Expression
}

var _ expression.VisitorContext = (*MapBasedVisitorContext)(nil)

func NewMapBasedParentVisitorContext() MapBasedVisitorContext {
	return MapBasedVisitorContext{
		parent:    nil,
		keyValMap: map[string]expression.Expression{},
	}
}

func NewMapBasedVisitorContext(parent expression.VisitorContext) MapBasedVisitorContext {
	return MapBasedVisitorContext{
		parent:    parent,
		keyValMap: map[string]expression.Expression{},
	}
}

func (ctx *MapBasedVisitorContext) IsDefined(key string) bool {
	if _, found := ctx.keyValMap[key]; found {
		return true
	}
	if ctx.parent != nil {
		return ctx.parent.IsDefined(key)
	}
	return false
}

func (ctx *MapBasedVisitorContext) IsDefinedLocally(key string) bool {
	_, found := ctx.keyValMap[key]
	return found
}

func (ctx *MapBasedVisitorContext) Lookup(key string) (expression.Expression, bool) {
	if val, found := ctx.keyValMap[key]; found {
		return val, true
	}
	if ctx.parent != nil {
		return ctx.parent.Lookup(key)
	}
	return nil, false
}

func (ctx *MapBasedVisitorContext) Define(key string, value expression.Expression) error {
	if _, found := ctx.keyValMap[key]; found {
		return fmt.Errorf("already defined")
	}
	ctx.keyValMap[key] = value
	return nil
}
