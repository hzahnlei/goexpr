package eval

import "zahnleiter.org/goexpr/pkg/expression"

func UInt8LiteralFromBool(isTrue bool) expression.UIntLiteral {
	if isTrue {
		return expression.NewUInt(uint8(1))
	} else {
		return expression.NewUInt(uint8(0))
	}
}
