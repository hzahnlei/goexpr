package eval

import (
	"zahnleiter.org/goexpr/pkg/expression"
)

type functionEvaluator struct {
	expression.BaseInfixVisitor
	originalSource expression.OptionalOriginalSource
}

var _ InfixEvaluator = (*functionEvaluator)(nil)

func newFunctionEvaluator(originalSource expression.OptionalOriginalSource) functionEvaluator {
	return functionEvaluator{
		originalSource: originalSource,
	}
}

// The infix operators evaluated herein can not be short circuited. So we first
// evaluate both - lhs and rhs - and then  apply the operator on both evaluated
// arguments.
func (eval functionEvaluator) evaluate(fun, argList expression.Expression, ctx expression.VisitorContext) (expression.Expression, error) {
	function, err := Evaluate(fun, ctx)
	if err != nil {
		return nil, err
	}
	argumentList, err := Evaluate(argList, ctx)
	if err != nil {
		return nil, err
	}
	return function.IfxAcceptWithUndetermined(eval, ctx, argumentList)
}
