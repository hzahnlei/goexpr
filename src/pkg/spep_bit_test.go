package goexpr_test

import (
	"testing"

	"github.com/stretchr/testify/require"
)

// ---- SPEP test = scan, parse, eval print test ------------------------------

func TestSPEPBitAndOperator(t *testing.T) {
	// GIVEN
	source := "0b10101010 & 0b11110000"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "160", result) // 160 = 0b10100000
}

func TestSPEPBitOrOperator(t *testing.T) {
	// GIVEN
	source := "0b10101010 | 0b11110000"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "250", result) // 250 = 0b1111101
}

func TestSPEPBitXOrOperator(t *testing.T) {
	// GIVEN
	source := "0b10101010 ^ 0b11110000"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "90", result) // 90 = 0b01011010
}

func TestSPEBitNegateOperator(t *testing.T) {
	// GIVEN
	source := "~0b01111110"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	// May seem surprising, but the input is signed after all!
	require.Equal(t, "129", result) // 0b100000001
}
