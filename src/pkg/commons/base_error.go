package commons

import (
	"fmt"
	"strings"
)

func MustNewLexicalError(msg string, fileName string, line, column int) locatableError {
	return mustNewLocatableError(msg, fileName, line, column, "Lexical")
}

func MustNewSyntaxError(msg string, fileName string, line, column int) locatableError {
	return mustNewLocatableError(msg, fileName, line, column, "Syntax")
}

func MustNewSemanticError(msg string, fileName string, line, column int) locatableError {
	return mustNewLocatableError(msg, fileName, line, column, "Semantic")
}

func MustNewRuntimeError(msg string, fileName string, line, column int) locatableError {
	return mustNewLocatableError(msg, fileName, line, column, "Runtime")
}

func MustNewUnlocatableRuntimeError(msg string) unlocatableError {
	return mustNewUnlocatableError(msg, "Runtime")
}

// ---- internal --------------------------------------------------------------

type locatableError struct {
	msg         string
	location    locationOfError
	displayName string
}

type locationOfError struct {
	fileName string
	line     int
	column   int
}

func mustNewLocatableError(msg string, fileName string, line, column int, displayName string) locatableError {
	checkedMsg := strings.TrimSpace(msg)
	if checkedMsg == "" {
		panic("Error message must not be empty.")
	}
	checkedDisplayName := strings.TrimSpace(msg)
	if checkedDisplayName == "" {
		panic("Display name must not be empty.")
	}
	return locatableError{
		msg: checkedMsg,
		location: locationOfError{
			fileName: strings.TrimSpace(fileName),
			line:     line,
			column:   column,
		},
		displayName: displayName,
	}
}

func (err locatableError) Error() string {
	if strings.TrimSpace(err.location.fileName) == "" {
		return fmt.Sprintf("%s error at line %d, column %d: %s",
			err.displayName, err.location.line, err.location.column, err.msg)
	} else {
		return fmt.Sprintf("%s error in %s at line %d, column %d: %s",
			err.displayName, err.location.fileName, err.location.line, err.location.column, err.msg)
	}
}

type unlocatableError struct {
	msg         string
	displayName string
}

func mustNewUnlocatableError(msg string, displayName string) unlocatableError {
	checkedMsg := strings.TrimSpace(msg)
	if checkedMsg == "" {
		panic("Error message must not be empty.")
	}
	checkedDisplayName := strings.TrimSpace(msg)
	if checkedDisplayName == "" {
		panic("Display name must not be empty.")
	}
	return unlocatableError{
		msg:         checkedMsg,
		displayName: displayName,
	}
}

func (err unlocatableError) Error() string {
	return fmt.Sprintf("%s error: %s", err.displayName, err.msg)
}
