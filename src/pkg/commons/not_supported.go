package commons

import "fmt"

func NotASupportedSInt(expr any) string {
	return fmt.Sprintf("Not a supported signed integer type: %#v.", expr)
}

func NotASupportedUInt(expr any) string {
	return fmt.Sprintf("Not a supported unsigned integer type: %#v.", expr)
}
