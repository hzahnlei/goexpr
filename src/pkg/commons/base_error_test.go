package commons

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestLocatableWithoutSourceFileName(t *testing.T) {
	// GIVEN
	err := locatableError{
		msg: "The message.",
		location: locationOfError{
			fileName: "",
			line:     1,
			column:   2},
		displayName: "Test",
	}
	// WHEN
	str := err.Error()
	// THEN
	require.Equal(t, "Test error at line 1, column 2: The message.", str)
}

func TestLocatablWithSourceFileName(t *testing.T) {
	// GIVEN
	err := locatableError{
		msg: "The message.",
		location: locationOfError{
			fileName: "test.txt",
			line:     1,
			column:   2},
		displayName: "Test",
	}
	// WHEN
	str := err.Error()
	// THEN
	require.Equal(t, "Test error in test.txt at line 1, column 2: The message.", str)
}

func TestUnlocatable(t *testing.T) {
	// GIVEN
	err := unlocatableError{
		msg:         "The message.",
		displayName: "Test",
	}
	// WHEN
	str := err.Error()
	// THEN
	require.Equal(t, "Test error: The message.", str)
}

func TestMustNewLocatableErrorWithoutMessage(t *testing.T) {
	assert.PanicsWithValue(t,
		"Error message must not be empty.",
		func() {
			mustNewLocatableError("", "main.txt", 10, 20, "syntax")
		})
}
