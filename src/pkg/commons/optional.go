package commons

type Optional[T any] struct {
	value *T
}

// Alternative constructor to expression "Optional[<type>]{value: &<value>}".
// Please note:  This constructor  copies the value.  Whereas the standard ini-
// tialisation expression uses a pointer.
// Example: OptionalOf[string]("abc")
func OptionalOf[T any](value T) Optional[T] {
	return Optional[T]{value: &value}
}

// Alternative constructor to expression "Optional[<type>]{value: nil}".
// Example: EmptyOptional[string]()
func EmptyOptional[T any]() Optional[T] {
	return Optional[T]{value: nil}
}

func (opt Optional[T]) HasValue() bool {
	return opt.value != nil
}

func (opt Optional[T]) Value() *T {
	return opt.value
}

func (opt Optional[T]) OrElse(alternativeVal T) *T {
	if opt.HasValue() {
		return opt.Value()
	}
	return &alternativeVal
}
