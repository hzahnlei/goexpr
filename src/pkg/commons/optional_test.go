package commons

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDirectlyConstructingEmptyOptional(t *testing.T) {
	// GIVEN
	empty := Optional[string]{}
	// WHEN
	// THEN
	require.False(t, empty.HasValue())
	assert.Nil(t, empty.Value())
}

func TestConstructingEmptyOptional(t *testing.T) {
	// GIVEN
	empty := EmptyOptional[string]()
	// WHEN
	// THEN
	require.False(t, empty.HasValue())
	assert.Nil(t, empty.Value())
}

func TestDirectlyConstructingNonEmptyOptional(t *testing.T) {
	// GIVEN
	value := "abc"
	nonempty := Optional[string]{&value}
	// WHEN
	// THEN
	require.True(t, nonempty.HasValue())
	require.NotNil(t, nonempty.Value())
	assert.Equal(t, value, *nonempty.Value())
}

func TestConstructingNonEmptyOptional(t *testing.T) {
	// GIVEN
	value := "abc"
	nonempty := OptionalOf(value)
	// WHEN
	// THEN
	require.True(t, nonempty.HasValue())
	require.NotNil(t, nonempty.Value())
	assert.Equal(t, value, *nonempty.Value())
}

func TestOrEleseTakesValueFromOptIfFresent(t *testing.T) {
	// GIVEN
	value := "abc"
	nonempty := OptionalOf(value)
	defaultVal := "xyz"
	// WHEN
	actual := nonempty.OrElse(defaultVal)
	// THEN
	require.Equal(t, value, *actual)
}

func TestOrEleseTakesDefaultValueFromOptIfEmpty(t *testing.T) {
	// GIVEN
	empty := EmptyOptional[string]()
	defaultVal := "xyz"
	// WHEN
	actual := empty.OrElse(defaultVal)
	// THEN
	require.Equal(t, defaultVal, *actual)
}
