package goexpr_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// ---- SPEP test = scan, parse, eval print test ------------------------------

func TestSPEPBoolAndOperator(t *testing.T) {
	assert.Equal(t, "0", evaluateWithoutContext("0 && 0"))
	assert.Equal(t, "0", evaluateWithoutContext("0 && 1"))
	assert.Equal(t, "0", evaluateWithoutContext("1 && 0"))
	assert.Equal(t, "1", evaluateWithoutContext("1 && 1"))
}

func TestSPEPBoolOrOperator(t *testing.T) {
	assert.Equal(t, "0", evaluateWithoutContext("0 || 0"))
	assert.Equal(t, "1", evaluateWithoutContext("0 || 1"))
	assert.Equal(t, "1", evaluateWithoutContext("1 || 0"))
	assert.Equal(t, "1", evaluateWithoutContext("1 || 1"))
}

func TestSPEPBoolXOrOperator(t *testing.T) {
	assert.Equal(t, "0", evaluateWithoutContext("0 ^^ 0"))
	assert.Equal(t, "1", evaluateWithoutContext("0 ^^ 1"))
	assert.Equal(t, "1", evaluateWithoutContext("1 ^^ 0"))
	assert.Equal(t, "0", evaluateWithoutContext("1 ^^ 1"))
}

func TestSPEPBoolNotOperator(t *testing.T) {
	assert.Equal(t, "0", evaluateWithoutContext("!1"))
	assert.Equal(t, "1", evaluateWithoutContext("!0"))
}
