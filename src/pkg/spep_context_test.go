package goexpr_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"zahnleiter.org/goexpr/pkg/eval"
	"zahnleiter.org/goexpr/pkg/expression"
)

// ---- SPEP test = scan, parse, eval print test ------------------------------

func TestSPEPIdentifierNotDefined(t *testing.T) {
	// GIVEN
	source := "undefined_identifier"
	emptyContext := eval.NewMapBasedParentVisitorContext()
	// WHEN
	result := evaluateWithContext(source, &emptyContext)
	// THEN
	require.Equal(t, `Runtime error at line 1, column 1: Identifier "undefined_identifier" is not defined.`, result)
}

func TestSPEPIdentifierDefined(t *testing.T) {
	// GIVEN
	source := "defined_identifier"
	nonEmptyContext := eval.NewMapBasedParentVisitorContext()
	nonEmptyContext.Define("defined_identifier", expression.NewUInt(uint32(1234567890)))
	// WHEN
	result := evaluateWithContext(source, &nonEmptyContext)
	// THEN
	require.Equal(t, "1234567890", result)
}

func TestSPEPComputeWithIdentifiers(t *testing.T) {
	// GIVEN
	source := "a * b"
	nonEmptyContext := eval.NewMapBasedParentVisitorContext()
	nonEmptyContext.Define("a", expression.NewUInt(uint8(4)))
	nonEmptyContext.Define("b", expression.NewUInt(uint8(6)))
	// WHEN
	result := evaluateWithContext(source, &nonEmptyContext)
	// THEN
	require.Equal(t, "24", result)
}
