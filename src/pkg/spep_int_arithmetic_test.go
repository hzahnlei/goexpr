package goexpr_test

import (
	"testing"

	"github.com/stretchr/testify/require"
)

// ---- SPEP test = scan, parse, eval print test ------------------------------

func TestSPEPDivideOperator(t *testing.T) {
	// GIVEN
	source := "6 / 5"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "1", result)
}

func TestSPEPMinusOperatorWithUnsignedIntegers(t *testing.T) {
	// GIVEN
	source := "3 - 5"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "254", result) // It's the two's complement of 2
}

func TestSPEPMinusOperatorWithSignedIntegers(t *testing.T) {
	// GIVEN
	source := "3s - 5s"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "-2", result)
}

func TestSPEPModulusOperator(t *testing.T) {
	// GIVEN
	source := "6 % 5"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "1", result)
}

func TestSPEPMultiplyOperator(t *testing.T) {
	// GIVEN
	source := "3 * 5"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "15", result)
}

func TestSPEPPlusOperator(t *testing.T) {
	// GIVEN
	source := "3 + 5"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "8", result)
}

func TestSPEPPowerOperator(t *testing.T) {
	// GIVEN
	source := "3 ** 5"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "243", result)
}

func TestSPEPShiftLeftOperator(t *testing.T) {
	// GIVEN
	source := "1 << 5"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "32", result)
}

func TestSPEPShiftRightOperator(t *testing.T) {
	// GIVEN
	source := "32 >> 5"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "1", result)
}

func TestSPEPArithNegationOperatorWithUnsignedInteger(t *testing.T) {
	// GIVEN
	source := "- 10"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "246", result) // It's actually the two's complement
}

func TestSPEPArithNegationOperatorWithSignedInteger(t *testing.T) {
	// GIVEN
	source := "- 10s"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "-10", result)
}
