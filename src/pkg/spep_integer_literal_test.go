package goexpr_test

import (
	"testing"

	"github.com/stretchr/testify/require"
)

// ---- SPEP test = scan, parse, eval print test ------------------------------

func TestSPEPSignedDecimalIntegerLiteral(t *testing.T) {
	// GIVEN
	source := "123"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "123", result)
}

func TestSPEPSignedHexadecimalIntegerLiteral(t *testing.T) {
	// GIVEN
	source := "0x123"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "0x123", result) // Prints original text representation if available
}

func TestSPEPIncompleteHexadecimalIntegerLiteral(t *testing.T) {
	// GIVEN
	source := "0x"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, `Lexical error at line 1, column 3: Premature end of hexadecimal number literal "0x".`, result)
}

func TestSPESignedBinaryIntegerLiteral(t *testing.T) {
	// GIVEN
	source := "0b10100101"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "0b10100101", result) // Prints original text representation if available
}

func TestSPEPIncompleteBinaryIntegerLiteral(t *testing.T) {
	// GIVEN
	source := "0b"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, `Lexical error at line 1, column 3: Premature end of binary number literal "0b".`, result)
}

func TestSPEPUnsignedDecimalIntegerLiteral(t *testing.T) {
	// GIVEN
	source := "123U"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "123U", result) // Prints original text representation if available
}

func TestSPEPUnsignedHexadecimalIntegerLiteral(t *testing.T) {
	// GIVEN
	source := "0xA9u8"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "0xA9u8", result)
}

func TestSPEPIncompleteUnsignedIntegerLiteral(t *testing.T) {
	// GIVEN
	source := "123U1"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "Lexical error at line 1, column 6: Premature end of 16 bit unsigned int. Expected '6' but reached end of file.", result)
}

func TestSPEPIllegalUnsignedIntegerLiteral(t *testing.T) {
	// GIVEN
	source := "123U13"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "Lexical error at line 1, column 6: Corrupt suffix of 16 bit unsigned int. Expected '6' but found '3'.", result)
}

func TestSPESUnsignedBinaryIntegerLiteral(t *testing.T) {
	// GIVEN
	source := "0b10100101U8"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "0b10100101U8", result) // Prints original text representation if available
}
