package goexpr_test

import (
	"testing"

	"github.com/stretchr/testify/require"
)

// ---- SPEP test = scan, parse, eval print test ------------------------------

func TestSPEPFloatDivideOperator(t *testing.T) {
	// GIVEN
	source := "6.0 / 5.0"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "1.200000", result)
}

func TestSPEPFloatMinusOperatorWithUnsignedIntegers(t *testing.T) {
	// GIVEN
	source := "3.0 - 5.0"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "-2.000000", result)
}

func TestSPEPFloatMinusOperatorWithSignedIntegers(t *testing.T) {
	// GIVEN
	source := "3.0 - 5.0"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "-2.000000", result)
}

func TestSPEPFloatModulusOperator(t *testing.T) {
	// GIVEN
	source := "6.0 % 5.0"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "Runtime error at line 1, column 1: Modulus operator '%' not applicable to floating point numbers.", result)
}

func TestSPEPFloatMultiplyOperator(t *testing.T) {
	// GIVEN
	source := "3.0 * 5.0"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "15.000000", result)
}

func TestSPEPFloatPlusOperator(t *testing.T) {
	// GIVEN
	source := "3.0 + 5.0"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "8.000000", result)
}

func TestSPEPFloatPowerOperator(t *testing.T) {
	// GIVEN
	source := "3.0 ** 5.0"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "243.000000", result)
}

func TestSPEPFloatShiftLeftOperator(t *testing.T) {
	// GIVEN
	source := "1.0 << 5.0"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "Runtime error at line 1, column 1: Shift left operator '<<' not applicable to floating point numbers.", result)
}

func TestSPEPFloatShiftRightOperator(t *testing.T) {
	// GIVEN
	source := "32.0 >> 5.0"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "Runtime error at line 1, column 1: Shift right operator '>>' not applicable to floating point numbers.", result)
}

func TestSPEPFloatArithNegationOperator(t *testing.T) {
	// GIVEN
	source := "- 10.0"
	// WHEN
	result := evaluateWithoutContext(source)
	// THEN
	require.Equal(t, "-10.000000", result)
}
