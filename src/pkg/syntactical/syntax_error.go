package syntactical

import (
	"zahnleiter.org/goexpr/pkg/commons"
	"zahnleiter.org/goexpr/pkg/lexical"
)

func syntaxError(msg string, location lexical.Location) error {
	return commons.MustNewSyntaxError(msg, location.SourceName, location.Line, location.Column)
}
