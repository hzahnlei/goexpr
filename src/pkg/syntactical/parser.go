package syntactical

import (
	"fmt"
	"math"
	"strconv"
	"unicode/utf8"

	"zahnleiter.org/goexpr/pkg/commons"
	"zahnleiter.org/goexpr/pkg/expression"
	"zahnleiter.org/goexpr/pkg/lexical"
)

type Parser struct {
	source   lexical.Scanner
	currExpr expression.Expression
}

func NewParser(scanner lexical.Scanner) (Parser, error) {
	parser := Parser{source: scanner, currExpr: expression.EOF{}}
	return parser, parser.Advance()
}

func (parser *Parser) Available() bool {
	return !parser.currExpr.IsEOF()
}

func (parser *Parser) Advance() error {
	expr, err := parser.parseSequence()
	if err != nil {
		return err
	}
	parser.currExpr = expr
	return nil
}

func (parser *Parser) currentToken() lexical.Token {
	return parser.source.Peek()
}

// Sequence (;)
func (parser *Parser) parseSequence() (expression.Expression, error) {
	lhs, err := parser.parseAssignment()
	if err != nil {
		return nil, err
	}
	if parser.currentToken().IsA(lexical.TokenSemicolon) {
		return parser.parseRestOfSequence(lhs)
	}
	return lhs, nil
}

func (parser *Parser) parseRestOfSequence(firstElement expression.Expression) (expression.Expression, error) {
	firstSemicolon := parser.currentToken()
	elements := []expression.Expression{firstElement}
	for parser.currentToken().IsA(lexical.TokenSemicolon) {
		parser.source.Advance() // Consume ;
		if !parser.source.Available() {
			break
		}
		nextElement, err := parser.parseAssignment()
		if err != nil {
			return nil, err
		}
		elements = append(elements, nextElement)
	}
	return expression.Sequence{
		Base: expression.Base{
			Origin: optionalOriginalSource(firstSemicolon),
		},
		Elements: elements,
	}, nil
}

// Assignement (:=)
func (parser *Parser) parseAssignment() (expression.Expression, error) {
	lhs, err := parser.parseList()
	if err != nil {
		return nil, err
	}
	if parser.currentToken().IsA(lexical.TokenAssignment) {
		operatorToken := parser.currentToken()
		operatorType := infixOpTypeOf(operatorToken) // :=
		if err := parser.source.Advance(); err != nil {
			return nil, err
		}
		rhs, err := parser.parseList()
		if err != nil {
			return nil, err
		}
		lhs = expression.Infix{
			Base: expression.Base{
				Origin: optionalOriginalSource(operatorToken),
			},
			OperatorType: operatorType,
			LHSArgument:  lhs,
			RHSArgument:  rhs,
		}
	}
	return lhs, nil
}

// List/vector/tuple (,)
func (parser *Parser) parseList() (expression.Expression, error) {
	lhs, err := parser.parseBooleanORorXOR()
	if err != nil {
		return nil, err
	}
	for parser.currentToken().IsA(lexical.TokenComma) {
		sequenceToken := parser.currentToken()
		parser.source.Advance() // Consume ,
		if !parser.source.Available() {
			return expression.List{
				Base: expression.Base{
					Origin: optionalOriginalSource(sequenceToken),
				},
				Head: lhs,
				Tail: nil,
			}, nil
		}
		rhs, err := parser.parseBooleanORorXOR()
		if err != nil {
			return nil, err
		}
		lhs = expression.List{
			Base: expression.Base{
				Origin: optionalOriginalSource(sequenceToken),
			},
			Head: lhs,
			Tail: rhs,
		}
	}
	return lhs, nil
}

// Boolean OR (||) and XOR (^^) have same priority
func (parser *Parser) parseBooleanORorXOR() (expression.Expression, error) {
	lhs, err := parser.parseBooleanAnd()
	if err != nil {
		return nil, err
	}
	for parser.currentToken().IsA(lexical.TokenBoolOr, lexical.TokenBoolXOr) {
		operatorToken := parser.currentToken()
		operatorType := infixOpTypeOf(operatorToken) // ^^ or ||
		if err := parser.source.Advance(); err != nil {
			return nil, err
		}
		rhs, err := parser.parseBooleanAnd()
		if err != nil {
			return nil, err
		}
		lhs = expression.Infix{
			Base: expression.Base{
				Origin: optionalOriginalSource(operatorToken),
			},
			OperatorType: operatorType,
			LHSArgument:  lhs,
			RHSArgument:  rhs,
		}
	}
	return lhs, nil
}

func infixOpTypeOf(token lexical.Token) expression.InfixOperatorType {
	mapping := map[lexical.TokenType]expression.InfixOperatorType{
		lexical.TokenArithDivide:     expression.InfixOpArithDivide,
		lexical.TokenArithMinus:      expression.InfixOpArithMinus,
		lexical.TokenArithModulus:    expression.InfixOpArithModulus,
		lexical.TokenArithMultiply:   expression.InfixOpArithMultiply,
		lexical.TokenArithPlus:       expression.InfixOpArithPlus,
		lexical.TokenArithPower:      expression.InfixOpArithPower,
		lexical.TokenArithShiftLeft:  expression.InfixOpArithShiftLeft,
		lexical.TokenArithShiftRight: expression.InfixOpArithShiftRight,
		lexical.TokenAssignment:      expression.InfixOpAssignment,
		lexical.TokenBitAnd:          expression.InfixOpBitAnd,
		lexical.TokenBitOr:           expression.InfixOpBitOr,
		lexical.TokenBitXOr:          expression.InfixOpBitXOr,
		lexical.TokenBoolAnd:         expression.InfixOpBoolAnd,
		lexical.TokenBoolOr:          expression.InfixOpBoolOr,
		lexical.TokenBoolXOr:         expression.InfixOpBoolXOr,
		lexical.TokenRelEQ:           expression.InfixOpRelEQ,
		lexical.TokenRelGE:           expression.InfixOpRelGE,
		lexical.TokenRelGT:           expression.InfixOpRelGT,
		lexical.TokenRelLE:           expression.InfixOpRelLE,
		lexical.TokenRelLT:           expression.InfixOpRelLT,
		lexical.TokenRelNE:           expression.InfixOpRelNE,
	}
	if op, found := mapping[token.Kind]; found {
		return op
	}
	panic(fmt.Sprintf("Unknown token %+v", token))
}

func optionalOriginalSource(token lexical.Token) expression.OptionalOriginalSource {
	source := expression.OriginalSource{
		Lexeme: token.Lexeme,
		Location: expression.OriginalLocation{
			FileName: token.Location.SourceName,
			Line:     token.Location.Line,
			Column:   token.Location.Column,
		},
	}
	return commons.OptionalOf(source)
}

func (parser *Parser) parseBooleanAnd() (expression.Expression, error) {
	lhs, err := parser.parseBooleanNot()
	if err != nil {
		return nil, err
	}
	for parser.source.Peek().Kind == lexical.TokenBoolAnd {
		operatorToken := parser.currentToken()
		if err := parser.source.Advance(); err != nil {
			return nil, err
		}
		rhs, err := parser.parseBooleanNot()
		if err != nil {
			return nil, err
		}
		lhs = expression.Infix{
			Base: expression.Base{
				Origin: optionalOriginalSource(operatorToken),
			},
			OperatorType: expression.InfixOpBoolAnd,
			LHSArgument:  lhs,
			RHSArgument:  rhs,
		}
	}
	return lhs, nil
}

func (parser *Parser) parseBooleanNot() (expression.Expression, error) {
	if parser.source.Peek().Kind == lexical.TokenBoolNot {
		operatorToken := parser.currentToken()
		if !parser.source.Available() {
			return nil, syntaxError("Premature end of boolean not expression. Expected expression but reached end of file.", parser.source.Peek().Location)
		}
		if err := parser.source.Advance(); err != nil {
			return nil, err
		}
		expr, err := parser.parseEquality()
		if err != nil {
			return nil, err
		}
		return expression.Prefix{
			Base: expression.Base{
				Origin: optionalOriginalSource(operatorToken),
			},
			OperatorType: expression.PrefixOpBoolNot,
			Argument:     expr,
		}, nil
	}
	return parser.parseEquality()
}

// Equality operators == and != have same priority
func (parser *Parser) parseEquality() (expression.Expression, error) {
	lhs, err := parser.parseRelational()
	if err != nil {
		return nil, err
	}
	for parser.currentToken().IsA(lexical.TokenRelEQ, lexical.TokenRelNE) {
		operatorToken := parser.currentToken()
		operatorType := infixOpTypeOf(operatorToken) // == or !=
		if err := parser.source.Advance(); err != nil {
			return nil, err
		}
		rhs, err := parser.parseRelational()
		if err != nil {
			return nil, err
		}
		lhs = expression.Infix{
			Base: expression.Base{
				Origin: optionalOriginalSource(operatorToken),
			},
			OperatorType: operatorType,
			LHSArgument:  lhs,
			RHSArgument:  rhs,
		}
	}
	return lhs, nil
}

// Relational operators <, <=, >, and >= have same priority
func (parser *Parser) parseRelational() (expression.Expression, error) {
	lhs, err := parser.parseAddition()
	if err != nil {
		return nil, err
	}
	for parser.currentToken().IsA(lexical.TokenRelLT, lexical.TokenRelLE, lexical.TokenRelGT, lexical.TokenRelGE) {
		operatorToken := parser.currentToken()
		operatorType := infixOpTypeOf(operatorToken) // <, <=, >, or >=
		if err := parser.source.Advance(); err != nil {
			return nil, err
		}
		rhs, err := parser.parseAddition()
		if err != nil {
			return nil, err
		}
		lhs = expression.Infix{
			Base: expression.Base{
				Origin: optionalOriginalSource(operatorToken),
			},
			OperatorType: operatorType,
			LHSArgument:  lhs,
			RHSArgument:  rhs,
		}
	}
	return lhs, nil
}

// Addition operators |, ^, -, and + have same priority
func (parser *Parser) parseAddition() (expression.Expression, error) {
	lhs, err := parser.parseMultiplication()
	if err != nil {
		return nil, err
	}
	for parser.currentToken().IsA(lexical.TokenBitOr, lexical.TokenBitXOr, lexical.TokenArithMinus, lexical.TokenArithPlus) {
		operatorToken := parser.currentToken()
		operatorType := infixOpTypeOf(operatorToken) // |, ^, -, or +
		if err := parser.source.Advance(); err != nil {
			return nil, err
		}
		rhs, err := parser.parseMultiplication()
		if err != nil {
			return nil, err
		}
		lhs = expression.Infix{
			Base: expression.Base{
				Origin: optionalOriginalSource(operatorToken),
			},
			OperatorType: operatorType,
			LHSArgument:  lhs,
			RHSArgument:  rhs,
		}
	}
	return lhs, nil
}

// Addition operators &, /, %, *, <<, and >> have same priority
func (parser *Parser) parseMultiplication() (expression.Expression, error) {
	lhs, err := parser.parsePower()
	if err != nil {
		return nil, err
	}
	for parser.currentToken().IsA(lexical.TokenBitAnd, lexical.TokenArithDivide, lexical.TokenArithModulus, lexical.TokenArithMultiply, lexical.TokenArithShiftLeft, lexical.TokenArithShiftRight) {
		operatorToken := parser.currentToken()
		operatorType := infixOpTypeOf(operatorToken) //  &, /, %, *, <<, or >>
		if err := parser.source.Advance(); err != nil {
			return nil, err
		}
		rhs, err := parser.parsePower()
		if err != nil {
			return nil, err
		}
		lhs = expression.Infix{
			Base: expression.Base{
				Origin: optionalOriginalSource(operatorToken),
			},
			OperatorType: operatorType,
			LHSArgument:  lhs,
			RHSArgument:  rhs,
		}
	}
	return lhs, nil
}

func (parser *Parser) parsePower() (expression.Expression, error) {
	lhs, err := parser.parseNegation()
	if err != nil {
		return nil, err
	}
	for parser.currentToken().Kind == lexical.TokenArithPower {
		operatorToken := parser.currentToken()
		if err := parser.source.Advance(); err != nil {
			return nil, err
		}
		rhs, err := parser.parseNegation()
		if err != nil {
			return nil, err
		}
		lhs = expression.Infix{
			Base: expression.Base{
				Origin: optionalOriginalSource(operatorToken),
			},
			OperatorType: expression.InfixOpArithPower,
			LHSArgument:  lhs,
			RHSArgument:  rhs,
		}
	}
	return lhs, nil
}

// Negation operators ~ and - have same priority
func (parser *Parser) parseNegation() (expression.Expression, error) {
	if parser.currentToken().IsA(lexical.TokenBitNot, lexical.TokenArithMinus) {
		operatorToken := parser.currentToken()
		operatorType := prefixOpTypeOf(operatorToken) //  ~ or -
		if !parser.source.Available() {
			return nil, syntaxError("Premature end of arithmetic or bitwise negation expression. Expected expression but reached end of file.", parser.source.Peek().Location)
		}
		if err := parser.source.Advance(); err != nil { // Consume '-' or '~'
			return nil, err
		}
		expr, err := parser.parseFunctionApplication()
		if err != nil {
			return nil, err
		}
		return expression.Prefix{
			Base: expression.Base{
				Origin: optionalOriginalSource(operatorToken),
			},
			OperatorType: operatorType,
			Argument:     expr,
		}, nil
	}
	return parser.parseFunctionApplication()
}

func prefixOpTypeOf(token lexical.Token) expression.PrefixOperatorType {
	return map[lexical.TokenType]expression.PrefixOperatorType{
		lexical.TokenArithMinus: expression.PrefixOpArithNegate,
		lexical.TokenBitNot:     expression.PrefixOpBitComplement,
		lexical.TokenBoolNot:    expression.PrefixOpBoolNot,
	}[token.Kind]
}

func (parser *Parser) parseFunctionApplication() (expression.Expression, error) {
	expr, err := parser.parsePrimary()
	if err != nil {
		return nil, err
	}
	for parser.source.Peek().IsA(lexical.TokenParenLeft) {
		expr, err = parser.parseArgumentList(expr)
		if err != nil {
			return nil, err
		}
	}
	return expr, nil
}

func (parser *Parser) parseArgumentList(funExpr expression.Expression) (expression.Expression, error) {
	parser.source.Advance() // Consume (
	if !parser.source.Available() {
		return nil, syntaxError("Premature end of argument list. Expected expression or ')' but reached end of file.", parser.source.Peek().Location)
	}
	argList, err := parser.parseList()
	if err != nil {
		return nil, err
	}
	if parser.currentToken().IsA(lexical.TokenEOF) {
		return nil, syntaxError("Premature end of argument list. Expected ')' but reached end of file.", parser.source.Peek().Location)
	}
	if !parser.currentToken().IsA(lexical.TokenParenRight) {
		return nil, syntaxError(
			fmt.Sprintf("Unexpected end of argument list. Expected ')' but found %q.", parser.source.Peek().Lexeme),
			parser.source.Peek().Location)
	}
	parser.source.Advance() // Consume )
	return expression.FunctionApplication{
		Base: expression.Base{
			Origin: funExpr.OriginalSource(),
		},
		Function:     funExpr,
		ArgumentList: argList,
	}, nil
}

func (parser *Parser) parsePrimary() (expression.Expression, error) {
	switch parser.source.Peek().Kind {
	case lexical.TokenEOF:
		return expression.EOF{}, nil
	case lexical.TokenParenLeft:
		return parser.parseParenExpr()
	case lexical.TokenIdentifier:
		return parser.parseIdentifierExpr()
	case lexical.TokenLiteralSInt8, lexical.TokenLiteralSInt16, lexical.TokenLiteralSInt32, lexical.TokenLiteralSInt64, lexical.TokenLiteralSInt:
		return parser.parseLiteralSIntExpr()
	case lexical.TokenLiteralUInt8, lexical.TokenLiteralUInt16, lexical.TokenLiteralUInt32, lexical.TokenLiteralUInt64, lexical.TokenLiteralUInt:
		return parser.parseLiteralUIntExpr()
	case lexical.TokenLiteralFloat:
		return parser.parseLiteralFloat()
	default:
		return nil, syntaxError(fmt.Sprintf("Not a primary expression: %q.", parser.source.Peek().Lexeme), parser.source.Peek().Location)
	}
}

func (parser *Parser) parseLiteralFloat() (expression.Expression, error) {
	floatToken := parser.source.Peek()
	value, err := strconv.ParseFloat(floatToken.Lexeme, 0)
	if err != nil {
		return nil, err
	}
	if err := parser.source.Advance(); err != nil { // Consume float literal
		return nil, err
	}
	return expression.FloatLiteral{
		Base: expression.Base{
			Origin: optionalOriginalSource(floatToken),
		},
		Value: value,
	}, nil
}

func (parser *Parser) parseParenExpr() (expression.Expression, error) {
	leftParenToken := parser.source.Peek()
	if err := parser.source.Advance(); err != nil { // Consume '('
		return nil, err
	}
	expr, err := parser.parseSequence()
	if err != nil {
		return nil, err
	}
	// TODO unite both cases (everywhere where applicable), peek returns EOF which is equivalent to "end of file"
	if parser.source.Peek().Kind == lexical.TokenEOF {
		return nil, syntaxError("Premature end of expression in parenthesis. Expected ')' but reached end of file.", parser.source.Peek().Location)
	}
	if parser.source.Peek().Kind != lexical.TokenParenRight {
		return nil, syntaxError(fmt.Sprintf(
			"Premature end of expression in parenthesis. Expected ')' but found %q.", parser.source.Peek().Lexeme),
			parser.source.Peek().Location)
	}
	if err := parser.source.Advance(); err != nil { // Consume ')'
		return nil, err
	}
	return expression.Parenthesis{
		Base: expression.Base{
			Origin: optionalOriginalSource(leftParenToken),
		},
		Expression: expr,
	}, nil
}

func (parser *Parser) parseIdentifierExpr() (expression.Expression, error) {
	identToken := parser.source.Peek()
	if err := parser.source.Advance(); err != nil { // Consume identifier
		return nil, err
	}
	return expression.Identifier{
		Base: expression.Base{
			Origin: optionalOriginalSource(identToken),
		},
		Name: identToken.Lexeme,
	}, nil
}

func (parser *Parser) parseLiteralSIntExpr() (expression.Expression, error) {
	sIntToken := parser.source.Peek()
	lexemeWithoutSSuffix := sIntToken.Lexeme[:posOfSSuffix(sIntToken.Lexeme)]
	value, err := strconv.ParseInt(lexemeWithoutSSuffix, 0, 0)
	if err != nil {
		return nil, err
	}
	if err := parser.source.Advance(); err != nil { // Consume integer literal
		return nil, err
	}
	return sintExprOfAppropriateSize(value, sIntToken)
}

func posOfSSuffix(lexeme string) int {
	for i, char := range lexeme {
		if char == 's' || char == 'S' {
			return i
		}
	}
	return utf8.RuneCountInString(lexeme)
}

func sintExprOfAppropriateSize(value int64, token lexical.Token) (expression.Expression, error) {
	switch token.Kind {
	case lexical.TokenLiteralSInt8:
		if value >= math.MinInt8 && value <= math.MaxInt8 {
			return expression.NewSIntWithOrigin(int8(value), optionalOriginalSource(token)), nil
		}
		return nil, semanticError(
			fmt.Sprintf("Value exceeds limit for eight bit signed. Expect %d ≤ value ≤ %d but is %d.", math.MinInt8, math.MaxInt8, value),
			token.Location)
	case lexical.TokenLiteralSInt16:
		if value >= math.MinInt16 && value <= math.MaxInt16 {
			return expression.NewSIntWithOrigin(int16(value), optionalOriginalSource(token)), nil
		}
		return nil, semanticError(
			fmt.Sprintf("Value exceeds limit for 16 bit signed. Expect %d ≤ value ≤ %d but is %d.", math.MinInt16, math.MaxInt16, value),
			token.Location)
	case lexical.TokenLiteralSInt32:
		if value >= math.MinInt32 && value <= math.MaxInt32 {
			return expression.NewSIntWithOrigin(int32(value), optionalOriginalSource(token)), nil
		}
		return nil, semanticError(
			fmt.Sprintf("Value exceeds limit for 32 bit signed. Expect %d ≤ value ≤ %d but is %d.", math.MinInt32, math.MaxInt32, value),
			token.Location)
	case lexical.TokenLiteralSInt64:
		return expression.NewSIntWithOrigin(int64(value), optionalOriginalSource(token)), nil
	default: // TokenLiteralSInt
		if value >= math.MinInt8 && value <= math.MaxInt8 {
			return expression.NewSIntWithOrigin(int8(value), optionalOriginalSource(token)), nil
		} else if value >= math.MinInt16 && value <= math.MaxInt16 {
			return expression.NewSIntWithOrigin(int16(value), optionalOriginalSource(token)), nil
		} else if value >= math.MinInt32 && value <= math.MaxInt32 {
			return expression.NewSIntWithOrigin(int32(value), optionalOriginalSource(token)), nil
		} else {
			return expression.NewSIntWithOrigin(int64(value), optionalOriginalSource(token)), nil
		}
	}
}

func (parser *Parser) parseLiteralUIntExpr() (expression.Expression, error) {
	uIntToken := parser.source.Peek()
	lexemeWithoutUSuffix := uIntToken.Lexeme[:posOfUSuffix(uIntToken.Lexeme)]
	value, err := strconv.ParseUint(lexemeWithoutUSuffix, 0, 0)
	if err != nil {
		return nil, err
	}
	if err := parser.source.Advance(); err != nil { // Consume integer literal
		return nil, err
	}
	return uintExprOfAppropriateSize(value, uIntToken)
}

func posOfUSuffix(lexeme string) int {
	for i, char := range lexeme {
		if char == 'u' || char == 'U' {
			return i
		}
	}
	return utf8.RuneCountInString(lexeme)
}

func uintExprOfAppropriateSize(value uint64, token lexical.Token) (expression.Expression, error) {
	switch token.Kind {
	case lexical.TokenLiteralUInt8:
		if value <= math.MaxUint8 {
			return expression.NewUIntWithOrigin(uint8(value), optionalOriginalSource(token)), nil
		}
		return nil, semanticError(
			fmt.Sprintf("Value exceeds limit for eight bit unsigned. Expect 0 ≤ value ≤ %d but is %d.", math.MaxUint8, value),
			token.Location)
	case lexical.TokenLiteralUInt16:
		if value <= math.MaxUint16 {
			return expression.NewUIntWithOrigin(uint16(value), optionalOriginalSource(token)), nil
		}
		return nil, semanticError(
			fmt.Sprintf("Value exceeds limit for 16 bit unsigned. Expect 0 ≤ value ≤ %d but is %d.", math.MaxUint16, value),
			token.Location)
	case lexical.TokenLiteralUInt32:
		if value <= math.MaxUint32 {
			return expression.NewUIntWithOrigin(uint32(value), optionalOriginalSource(token)), nil
		}
		return nil, semanticError(
			fmt.Sprintf("Value exceeds limit for 32 bit unsigned. Expect 0 ≤ value ≤ %d but is %d.", math.MaxUint32, value),
			token.Location)
	case lexical.TokenLiteralUInt64:
		return expression.NewUIntWithOrigin(uint64(value), optionalOriginalSource(token)), nil
	default: // TokenLiteralUInt
		if value <= math.MaxUint8 {
			return expression.NewUIntWithOrigin(uint8(value), optionalOriginalSource(token)), nil
		} else if value <= math.MaxUint16 {
			return expression.NewUIntWithOrigin(uint16(value), optionalOriginalSource(token)), nil
		} else if value <= math.MaxUint32 {
			return expression.NewUIntWithOrigin(uint32(value), optionalOriginalSource(token)), nil
		} else {
			return expression.NewUIntWithOrigin(uint64(value), optionalOriginalSource(token)), nil
		}
	}
}

func (parser *Parser) Peek() expression.Expression {
	return parser.currExpr
}
