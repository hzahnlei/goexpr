package syntactical

import (
	"zahnleiter.org/goexpr/pkg/commons"
	"zahnleiter.org/goexpr/pkg/lexical"
)

func semanticError(msg string, location lexical.Location) error {
	return commons.MustNewLexicalError(msg, location.SourceName, location.Line, location.Column)
}
