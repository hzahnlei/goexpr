package syntactical

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"zahnleiter.org/goexpr/pkg/expression"
	"zahnleiter.org/goexpr/pkg/lexical"
)

func TestParsingPrimaryExpressions(t *testing.T) {
	source := lexical.NewSource("(123) 456 abc", "")
	scanner, err := lexical.NewScanner(source)
	require.NoError(t, err)
	parser, err := NewParser(scanner)
	require.NoError(t, err)

	expr := parser.Peek()
	parenExpr, ok := expr.(expression.Parenthesis)
	require.True(t, ok)
	intExpr, ok := parenExpr.Expression.(expression.UIntLiteral)
	require.True(t, ok)
	assert.Equal(t, uint8(123), intExpr.Value)

	err = parser.Advance()
	require.NoError(t, err)
	expr = parser.Peek()
	intExpr, ok = expr.(expression.UIntLiteral)
	require.True(t, ok)
	assert.Equal(t, uint16(456), intExpr.Value)

	err = parser.Advance()
	require.NoError(t, err)
	expr = parser.Peek()
	identExpr, ok := expr.(expression.Identifier)
	require.True(t, ok)
	assert.Equal(t, "abc", identExpr.Name)

	err = parser.Advance()
	require.NoError(t, err)
	_, ok = parser.Peek().(expression.EOF)
	require.True(t, ok)
}
