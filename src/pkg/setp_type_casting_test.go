package goexpr_test

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCastUInt8ToUInt8(t *testing.T) {
	// GIVEN
	source := "u8(10)"
	// WHEN
	result := evaluateWithDefaultContext(source)
	// THEN
	require.Equal(t, "10", result)
}

func TestCastSInt8ToUInt8(t *testing.T) {
	// GIVEN
	source := "u8(-10s)"
	// WHEN
	result := evaluateWithDefaultContext(source)
	// THEN
	require.Equal(t, "246", result) // Two's complement
}

func TestCastSInt8ToUInt16(t *testing.T) {
	// GIVEN
	source := "u16(-10s)"
	// WHEN
	result := evaluateWithDefaultContext(source)
	// THEN
	require.Equal(t, "65526", result) // Two's complement again, but 16 bit now
}

func TestCastUInt8ToSInt16(t *testing.T) {
	// GIVEN
	source := "s16(11)"
	// WHEN
	result := evaluateWithDefaultContext(source)
	// THEN
	require.Equal(t, "11", result) // Two's complement
}

func TestCastSInt16ToSInt16(t *testing.T) {
	// GIVEN
	source := "s16(-11s16)"
	// WHEN
	result := evaluateWithDefaultContext(source)
	// THEN
	require.Equal(t, "-11", result) // Two's complement
}

func TestDifferenceBetweenUncastedAndCasted(t *testing.T) {
	// GIVEN
	source := "~10"
	// WHEN
	result := evaluateWithDefaultContext(source)
	// THEN
	require.Equal(t, "245", result)

	// GIVEN
	source = "~s8(10)"
	// WHEN
	result = evaluateWithDefaultContext(source)
	// THEN
	require.Equal(t, "-11", result)
}

func TestCastSInt8ToFloat64(t *testing.T) {
	// GIVEN
	source := "f64(123s)"
	// WHEN
	result := evaluateWithDefaultContext(source)
	// THEN
	require.Equal(t, "123.000000", result) // Two's complement
}

func TestCastUInt8ToFloat64(t *testing.T) {
	// GIVEN
	source := "f64(123u)"
	// WHEN
	result := evaluateWithDefaultContext(source)
	// THEN
	require.Equal(t, "123.000000", result) // Two's complement
}

func TestCastFloat64ToFloat64(t *testing.T) {
	// GIVEN
	source := "f64(123.0)"
	// WHEN
	result := evaluateWithDefaultContext(source)
	// THEN
	require.Equal(t, "123.000000", result) // Two's complement
}
