package formatter

import (
	"fmt"
	"strings"

	"zahnleiter.org/goexpr/pkg/commons"
	"zahnleiter.org/goexpr/pkg/expression"
)

// A formatter that is used by a REPL to print the result of an evaluation.  It
// does not do any complex indenting nor aligning. The assumption is, that eva-
// luated results are simple and can be printed "flat".
type SimpleResultPrinter struct {
	builder strings.Builder
}

var _ Formatter = (*SimpleResultPrinter)(nil)
var _ expression.Visitor = (*SimpleResultPrinter)(nil)

func NewResultPrinter() SimpleResultPrinter {
	return SimpleResultPrinter{}
}

func (d *SimpleResultPrinter) Format(expr expression.Expression) string {
	_, _ = expr.Accept(d, nil)
	return d.builder.String()
}

func (d *SimpleResultPrinter) VisitEOF(expression.EOF, expression.VisitorContext) (expression.Expression, error) {
	return nil, nil
}

func (d *SimpleResultPrinter) VisitIdentifier(expr expression.Identifier, ctx expression.VisitorContext) (expression.Expression, error) {
	d.builder.WriteRune(' ')
	d.builder.WriteString(expr.Name)
	return nil, nil
}

func (d *SimpleResultPrinter) VisitInfixExpression(expr expression.Infix, ctx expression.VisitorContext) (expression.Expression, error) {
	var infixOpStrMapping = map[expression.InfixOperatorType]string{
		expression.InfixOpArithDivide: "/",
		expression.InfixOpArithMinus:  "-",
		expression.InfixOpArithPlus:   "+",
		expression.InfixOpBoolAnd:     "&&",
		expression.InfixOpBoolOr:      "||",
		expression.InfixOpBoolXOr:     "^^",
	}

	expr.LHSArgument.Accept(d, nil)
	d.builder.WriteRune(' ')
	d.builder.WriteString(infixOpStrMapping[expr.OperatorType])
	expr.RHSArgument.Accept(d, nil)
	return nil, nil
}

func (d *SimpleResultPrinter) VisitSIntLiteral(expr expression.SIntLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	switch val := expr.Value.(type) {
	case int8, int16, int32, int64:
		d.builder.WriteString(
			*expr.OriginalRepresentationIfPresent().
				OrElse(fmt.Sprintf("%d", val)))
	default:
		panic(commons.NotASupportedSInt(expr)) // Should never happen
	}
	return nil, nil
}

func (d *SimpleResultPrinter) VisitUIntLiteral(expr expression.UIntLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	switch val := expr.Value.(type) {
	case uint8, uint16, uint32, uint64:
		d.builder.WriteString(
			*expr.OriginalRepresentationIfPresent().
				OrElse(fmt.Sprintf("%d", val)))
	default:
		panic(commons.NotASupportedUInt(expr)) // Should never happen
	}
	return nil, nil
}

func (d *SimpleResultPrinter) VisitParenthesis(expr expression.Parenthesis, ctx expression.VisitorContext) (expression.Expression, error) {
	d.builder.WriteRune('(')
	expr.Accept(d, nil)
	d.builder.WriteRune(')')
	return nil, nil
}

func (d *SimpleResultPrinter) VisitPrefixExpression(expr expression.Prefix, ctx expression.VisitorContext) (expression.Expression, error) {
	// TODO check duplication
	var prefixOpStrMapping = map[expression.PrefixOperatorType]string{
		expression.PrefixOpArithNegate:   "-",
		expression.PrefixOpBitComplement: "~",
		expression.PrefixOpBoolNot:       "!",
	}

	d.builder.WriteRune(' ')
	d.builder.WriteString(prefixOpStrMapping[expr.OperatorType])
	expr.Argument.Accept(d, nil)
	return nil, nil
}

func (d *SimpleResultPrinter) VisitSequence(expr expression.Sequence, ctx expression.VisitorContext) (expression.Expression, error) {
	for i, element := range expr.Elements {
		if i > 0 {
			d.builder.WriteString("; ")
		}
		element.Accept(d, ctx)
	}
	return nil, nil
}

func (d *SimpleResultPrinter) VisitList(expr expression.List, ctx expression.VisitorContext) (expression.Expression, error) {
	expr.Head.Accept(d, nil)
	if expr.Tail != nil {
		d.builder.WriteString(", ")
		expr.Tail.Accept(d, nil)
	}
	return nil, nil
}

func (d *SimpleResultPrinter) VisitFunctionApplication(expr expression.FunctionApplication, ctx expression.VisitorContext) (expression.Expression, error) {
	expr.Function.Accept(d, nil)
	d.builder.WriteRune('(')
	expr.ArgumentList.Accept(d, nil)
	d.builder.WriteRune(')')
	return nil, nil
}

func (d *SimpleResultPrinter) VisitBuiltinFunction(expr expression.BuiltinFunction, ctx expression.VisitorContext) (expression.Expression, error) {
	d.builder.WriteString(expr.Name)
	return nil, nil
}

func (d *SimpleResultPrinter) VisitFloatLiteral(expr expression.FloatLiteral, ctx expression.VisitorContext) (expression.Expression, error) {
	d.builder.WriteString(fmt.Sprintf("%f", expr.Value))
	return nil, nil
}
