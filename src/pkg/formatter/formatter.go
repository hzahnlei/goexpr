package formatter

import "zahnleiter.org/goexpr/pkg/expression"

type Formatter interface {
	Format(expr expression.Expression) string
}
