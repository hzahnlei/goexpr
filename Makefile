PHONY: clean build test demo run

.EXPORT_ALL_VARIABLE:
SW_VERSION ?= $(shell git describe --tags)
GO_LD_FLAGS := $(GO_LD_FLAGS) -s -w -X 'main.version=$(SW_VERSION)'
GO_SRC_DIR ?= $(shell pwd)/src
GO_BUILD_DIR ?= $(shell pwd)/.build
GO_PKG_DIR = $(GO_SRC_DIR)/pkg
COVERED_PACKAGES = ./pkg/commons,./pkg/eval,./pkg/expression,./pkg/formatter,./pkg/lexical,./pkg/syntactical
REPORTS_DIR = $(shell pwd)/.reports

clean:
	rm -rf "$(GO_BUILD_DIR)"
	rm -rf "$(REPORTS_DIR)"

build:
	rm -rf "$(GO_BUILD_DIR)"
	cd "$(GO_SRC_DIR)/cmd" && \
		CGO_ENABLED=0 \
		go build \
	   	   -ldflags "$(GO_LD_FLAGS)" \
	   	   -o "$(GO_BUILD_DIR)/goexpr" .

# -race not really required for this library, using it by default however
test:
	rm -rf "$(REPORTS_DIR)"
	mkdir "$(REPORTS_DIR)"
	cd "$(GO_SRC_DIR)" && \
		go test -v -race \
		        -coverprofile "$(REPORTS_DIR)/coverage.out" \
		        -coverpkg "$(COVERED_PACKAGES)" \
		        ./... > "$(REPORTS_DIR)/test.out" && \
		go tool cover -html="$(REPORTS_DIR)/coverage.out" -o "$(REPORTS_DIR)/coverage.html" && \
		go tool cover -func="$(REPORTS_DIR)/coverage.out"

demo: build
	echo '10 + 3 * 3' | "$(GO_BUILD_DIR)/goexpr"

run: build
	"$(GO_BUILD_DIR)/goexpr"
