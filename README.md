# GoExpr - Go Expression Interpreter Library

A direct interpreter library written in Go for parsing and interpreting expressions.

GoExpr can be used in two scenarios:

1. As a library in your Go programs, if you want to dynamically evaluate expressions
2. As a command line tool for evaluating expressions in shell scripts.
   This project comes with a command line application.
   Its main purpose is to demonstrate the use of the library.
   It can, however, be invoked from shell scripts of cause

Holger Zahnleiter,<br>
2024-06-21

## Folder Structure

A project's root folder gets clutterd relatively quickly.
There are ignore files, license files, read-me files, CI files, and what not.
I prefer the root folder to contain as few files and folders as possible.
Therefore, all Go source code and the module file reside in the `src` folder.
For the same reason I name the build folder `.build` instead of `build`.

## Supported Data Types

### Integer Data Types

Supported integer data types are:

* 8, 16, 32, and 64 bit signed integer
* 8, 16, 32, and 64 bit unsigned integer

Integer number literals can be constrained to a certain size by the following suffixes (not case sensitive):

* `u8`/`U8` an 8 bit unsigned integer (aka byte)
* `u16`/`U16` a 16 bit unsigned integer
* `u32`/`U32` a 32 bit unsigned integer
* `u64`/`U64` a 64 bit unsigned integer
* `u`/`U` a unsigned integer of the smallest size to hold the given value
* `s8`/`S8` an 8 bit signed integer (aka byte)
* `s16`/`S16` a 16 bit signed integer
* `s32`/`S32` a 32 bit signed integer
* `s64`/`S64` a 64 bit signed integer
* `s`/`S` a signed integer of the smallest size to hold the given value

Integer literals can be written in binary form (`0b` or `0B` prefix), decimal form (no prefix), and hexadecimal form (`0x` or `0X` prefix.)
Here are some examples for integer number literals:

* `123` an 8 bit unsigned integer in decimal notation
  * `0b01111001`, `0B01111001u`, and `0b01111001U8` are equivalent binary notations
  * `123u`, `123U`, `123u8`, and `123U8` are equivalent decimal notations
  * `0x7B`, `0x7bU`, and `0x7bU8` are equivalent hexadecimal notations
* `1234` a 16 bit unsigned integer in decimal notation
  * `0b01111001`, `0B01111001u`, and `0b01111001U8` are equivalent binary notations
  * `1234u`, `1234U`, `1234u16`, and `1234U16` are equivalent decimal notations
* `1u64` is a 64 bit unsigned integer
* `1s16` and `123S16` are examples for 16 bit signed integers

| Type             | Suffix | Size (Bits) | Domain                                                  |
| ---------------- | ------ | ----------: | ------------------------------------------------------- |
| signed integer   | `S8`   |           8 | -128 to 127 |
| signed integer   | `S16`  |          16 | -32,768 to 32,767 |
| signed integer   | `S32`  |          32 | -2,147,483,648 to 2,147,483,647|
| signed integer   | `S64`  |          64 | -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807 |
| unsigned integer | `U8`   |           8 | 0 to 255 |
| unsigned integer | `U16`  |          16 | 0 to 65,535 |
| unsigned integer | `U32`  |          32 | 0 to 4,294,967,295 |
| unsigned integer | `U64`  |          64 | 0 to 18,446,744,073,709,551,615 |

These numeric datatypes are backed by corresponding datatypes from the Go language.
See [The Go Programming Language Specification](https://go.dev/ref/spec#Numeric_types) for details.

### Floating Point Data Types

The only supported floating point data type is a 64 bit float.
Numberliterals with an decimal point will be interpreted as float.
For example, `123.00` is a floating point number of 64 bits.

Currently only the decimal notation is supported.
Currently there is no support for binary or hexadecimal notation.

Float suffixes are not supported neither required as there is only on supported float type at the time of this writing.

The followning floating point constants are predefined for your convenience.

| Name      | Value    | Remarks                                     |
| --------- | -------: | ------------------------------------------- |
| `pi`, `π` | 3.141593 | Equals `math.Pi` from Go's `math` package ` |
| `e`       | 2.718282 | Equals `math.E` from Go's `math` package `  |

## Operators

### Table Of Operators

| Precedence | Infix/Prefix | Symbol | Short Circuit | Explanation                 |
| ---------: | :----------- | :----- | :------------ | :-------------------------: |
|          0 | Infix        | `:=`   | no            | Assignment                  |
|          1 | Infix        | `\|\|` | yes           | Boolean or                  |
|          1 | Infix        | `^^`   | no            | Boolean xor                 |
|          2 | Infix        | `&&`   | yes           | Boolean and                 |
|          3 | Prefix       | `!`    | no            | Boolean not                 |
|          4 | Infix        | `==`   | no            | Relational equal            |
|          4 | Infix        | `!=`   | no            | Relational not equal        |
|          5 | Infix        | `<`    | no            | Relational less than        |
|          5 | Infix        | `<=`   | no            | Relational less or equal    |
|          5 | Infix        | `>=`   | no            | Relational greater than     |
|          5 | Infix        | `>`    | no            | Relational greater or equal |
|          6 | Infix        | `+`    | no            | Arithmetic addition         |
|          6 | Infix        | `-`    | no            | Arithmetic subtraction      |
|          6 | Infix        | `\|`   | no            | Bitwise or                  |
|          6 | Infix        | `^`    | no            | Bitwise xor                 |
|          7 | Infix        | `*`    | no            | Arithmetic multiplication   |
|          7 | Infix        | `/`    | yes           | Arithmetic division         |
|          7 | Infix        | `%`    | no            | Arithmetic modulus          |
|          7 | Infix        | `<<`   | no            | Arithmetic shift left       |
|          7 | Infix        | `>>`   | no            | Arithmetic shift right      |
|          7 | Infix        | `&`    | no            | Bitwise and                 |
|          8 | Infix        | `**`   | no            | Arithmetic power            |
|          9 | Prefix       | `-`    | no            | Arithmetic negation         |
|          9 | Prefix       | `~`    | no            | Bitwise (one's) complement  |

Short circuiting means, that the second argument (aka right hand side, RHS for short) to an infix expression may not be evaluated, depending on the value of the first argument (aka left hand side, LHS for short).

In the case of short circuiting the division operator we evaluate RHS first.
If RHS evaluates to zero (0), then we issue an "division by zero" error and do not evaluate LHS.
Otherwise LHS is evaluated and the result will be LHS divided by RHS.

### Numeric Operators

The numeric opetarors require both operands to be of the same size.
The operand of the bigger size determines the size of the operation.
The result will be of the bigger size.

For example: One may add `10U8 + 1U64`.
`1U64` will be added to `10U8` giving a result of 11 in form of a 64 bit unsigned integer (`11U64`).

If you subtract `3U8 - 5U8` (=254, 8 bit unsigned) you might be surprised by the result.
You may have expected a negative number -2.
However the data type does not change.
The result is the unsigned 8 bit two's complement of -2, which is 254.

### Type Casting

Numeric literals can be associated with a specific type by adding an suffix such as `U8` or `S16`.
But what about computed values?
Computed values can by cast by applying type casting functions.

| Function | Signature(s)  | 
| :------- | :------------ |
| `u<X>`   | `u<Y> ➝ u<X>` |
|          | `s<Y> ➝ u<X>` |
| `s<X>`   | `u<Y> ➝ s<X>` |
|          | `s<Y> ➝ s<X>` |
| `f64`    | `u<Y> ➝ f64`  |
|          | `s<Y> ➝ f64`  |

Let X ∈ {8, 16, 32, 64} and Y ∈ {8, 16, 32, 64}.

For example, `u8(5*4)` will be an unsigned integer of eight bytes.

### Boolean Operators and Expressions

There is no explicit boolean type.
All boolean expressions work on numbers.
A value of zero (=0) is interpreted as a logical false.
All other values not equal to zero (≠0) are treated as logical true.

By default two "boolean" constants have been predefined: `true` (=1) and `false` (=0).
Both are eight bit unsigned integers.

### Expression Sequences

Expressions can become complex some times.
(Even though overly complex expressions are not the main target of this library.)
Therefore, sequences of expressions can be built using the sequencing operator `;` (semicolon).

Sequences are similar to `let` expressions in functional languages.
You can use them to compute intermediate results.
The evaluation result of the sequence will be the last expression of the sequence.
Here is an example: `a:=4*5; b:=3*2; a+b`.
The sequence will evaluate to 26.

**Important note**: `a` and `b` are only visible within the sequence.
They will not be defined globally and will not be available to subsequent expressions.

If `a` and `b` were supposed to be availabe to other expressions, then they need to be defined on the top level like so: `a:=4*5  b:=3*2  a+b`.
The values of `a` and `b` are defined at the top level and thus available for all subsequent expression.

## Functional Principle Of The Interpreter

The interpreter employs the visitor pattern for evaluating expressions.
The well known visitor pattern is sufficient on the top level.

The parser produces an expression `expr` of the unspecific type `Expression`.
`expr` gets passed to the evaluator `eval` which first needs to find out what type of expression `expr` is.
Only with this knowledge can the evaluator decide what to do with `expr`.
(`expr` of cause knows what it is.)

Evaluator and expressions are implementing the visitor pattern.
The evaluator invokes `expr.accept(eval)`.
Let's assume `expr` to be an integer literal expression.
The expression calls back.
It does so by calling `eval`'s `visit` that is specific for expressions of type `IntegerExpression`: `eval.visit(expr)`

Now, `eval`'s `visit` knows that `expr` is an integer literal and knows what to do with it.

```mermaid
sequenceDiagram
    participant PROG as <<program>><br>REPL
    participant EVAL as <<visitor>><br>eval: Evaluator
    participant EXPR as <<visitable>><br>expr: IntegerExpression
    PROG->>+EVAL: eval.evaluate(expr: Expression)
    EVAL->>+EXPR: expr.accept(eval: Visitor)
    EXPR-->>-EVAL: eval.visit(expr: IntegerExpression)
    EVAL->>EVAL: evaluate<br>integer<br>expression
    EVAL-->>-PROG: result
```
